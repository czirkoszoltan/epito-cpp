A menü objektum lenne példányosítva a programban, ez vezérel mindent:

Menu app;
app.fomenu();





Ez tartalmazhatná a jelenleg globális éllistát,
mert minden menüs függvény azt amúgy is látja:



class Menu {
    vector<act_ellistas> ellistas;
    
    
    public:
        void fomenu() {
            std::cout << " " << std::endl << std::endl;
            std::cout << "Select project: " << std::endl;
            std::cout << "1- Read from drive        (responsible: HM)" << std::endl;
            std::cout << "2- Import from MS Project (responsible: VZ)" << std::endl;
            std::cout << "3- Generate network       (responsible: VZ)" << std::endl;
            std::cout << "6- Go to calculations     (responsible: ML, HM, VZ, LA, VI)" << std::endl;

            What_To_Do = Integer_Read(1, 7);
            switch (What_To_Do)
            {
                case 1:
                {
                    READING_WRITING_HARD_DRIVE alg1;
                    
                    ellistas = read_from_file(...);
                }
                case 6:
                {
                    calc_almenu();
                }
            }
        }
        
        void calc_almenu() {
            std::cout << " " << std::endl << std::endl;
            std::cout << "Select project: " << std::endl;
            std::cout << "1- Dijkstra        (responsible: HM)" << std::endl;
            std::cout << "2- BF (responsible: VZ)" << std::endl;
            
            case 1
                dijkstra_almenu();
        }
        
        void dijkstra_almenu() {
            std::cout << " " << std::endl << std::endl;
            std::cout << "Select project: " << std::endl;
            std::cout << "1- Dijkstra v1";
            std::cout << "2- Dijkstra v2";
            
            case 2: {
                DIJKSTRA_MIN_SSE_VECTOR alg1;
                alg1.felepit_es_kiszamit(this->ellistas);
            }
        }
};

