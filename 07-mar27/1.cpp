// auto és range-based for

#include <iostream>
#include <vector>
#include <string>

std::vector<std::string> sorokat_beolvas() {
    std::vector<std::string> v;
    std::string temp;
    std::cout << "Írj be szavakat, végére üres sort!\n";
    while (true) {
        std::getline(std::cin, temp);
        if (temp == "")
            break;
        v.push_back(temp);
    }
    return v;
}

int main() {
    std::vector<std::string> v = sorokat_beolvas();
    
    // range-based for loop
    for (auto & s : v)
        s += "!";
    
    for (auto const & s : v)
        std::cout << s << std::endl;
}
