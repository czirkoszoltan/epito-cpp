#include <map>
#include <iostream>

int main() {
    // melyik szó hányszor szerepel?
    // sztring => integer leképezés
    std::map< std::string, int >  db;
    
    std::string szo;
    while (std::cin >> szo)
        db[szo] += 1;
    
    for (auto & par : db) {     // pár   std::pair< std::string, int >
        std::cout << par.first << " " << par.second << std::endl;
    }
}
