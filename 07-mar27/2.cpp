// memóriacímek

#include <iostream>
#include <cstdint>

double glob[5];

int main() {
    uint8_t loc[5];
    
    int *heap = new int[5];

    
    for (size_t i = 0; i < 5; ++i)
        std::cout << (void*) &loc[i] << std::endl;

    for (size_t i = 0; i < 5; ++i)
        std::cout << &glob[i] << std::endl;

    for (size_t i = 0; i < 5; ++i)
        std::cout << &heap[i] << std::endl;
    
}
