// szálkezelés: async és future

#include <iostream>
#include <thread>
#include <unistd.h>
#include <vector>
#include <mutex>
#include <map>
#include <future>       // std::future

// külön szál, párhuzamos!!!
int negyzet(int x) {
    int eredmeny = x*x;
    sleep(1);   // számítás

    return eredmeny;
}


int main() {
    std::vector<std::future<int>> v;
    
    for (int i = 1; i <= 20; ++i) {
        // async: külön szálon elindítja,
        // és egy ún. future objektumot ad vissza,
        // amitől az eredmény később elkérhető
        v.push_back( std::async( std::launch::async, negyzet, i ) );
    }

    // itt kérjük el az eredményeket, a szálak
    // bevárása után. a get() függvény azt adja
    // vissza, amit a szálként indított függvény adott.
    for (auto & f : v)
        std::cout << f.get() << std::endl;
}





