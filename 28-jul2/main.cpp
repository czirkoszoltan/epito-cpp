// GTK+  ->  C
// gtkmm  ->  C++


#include <iostream>
#include <sstream>
#include <gtk/gtk.h>

class CounterWindow {
    private:
        int cnt = 0;
        GtkWidget *label;

        static void bezar_clicked(GtkWidget *widget, void *data) {
            std::cout << "clicked" << std::endl;
            gtk_main_quit();
        }

        static void window_close(GtkWidget *widget, void *data) {
            gtk_main_quit();
        }

        static void plusone_clicked(GtkWidget *widget, void *data) {
            CounterWindow *c1 = (CounterWindow *) data;
            c1->cnt += 1;
            std::ostringstream os;
            os << c1->cnt;
            gtk_label_set_label(GTK_LABEL(c1->label), os.str().c_str());
                                                    //os -> std::ostringstream
                                                    //os.str() -> std::string
                                                    //os.str().c_str() -> char*
        }

    public:
        CounterWindow() {
            GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
            
            gtk_window_set_title(GTK_WINDOW(window), "GTK+ Példaprogram");
                            //   ^^^^^^^^^^^^^^^^^^
                            // GtkWidget* -> GtkWindow*
            
            //~ GtkWidget *button = gtk_button_new();
            //~ GtkWidget *label = gtk_label_new("Első gomb");
            //~ gtk_container_add(GTK_CONTAINER(button), label);
            GtkWidget *close = gtk_button_new_with_mnemonic("_Bezár");
            
            // "glib"
            g_signal_connect(G_OBJECT(close), "clicked", G_CALLBACK(bezar_clicked), NULL);
            g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(window_close), NULL);
            
            label = gtk_label_new("helló");
            GtkWidget *plusone = gtk_button_new_with_label("+1");
            GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
            
            g_signal_connect(G_OBJECT(plusone), "clicked", G_CALLBACK(plusone_clicked), this);
            
            gtk_container_add(GTK_CONTAINER(vbox), label);
            gtk_container_add(GTK_CONTAINER(vbox), plusone);
            gtk_container_add(GTK_CONTAINER(vbox), close);
            
            gtk_container_add(GTK_CONTAINER(window), vbox); 
            gtk_widget_set_margin_start(vbox, 16);
            gtk_widget_set_margin_end(vbox, 16);
            gtk_widget_set_margin_top(vbox, 16);
            gtk_widget_set_margin_bottom(vbox, 16);
            
            gtk_widget_show_all(window);
        }
};


int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);

    CounterWindow cw;
    
    gtk_main();     // eseménykezelő
    
    std::cout << "Program vége" << std::endl;
}
