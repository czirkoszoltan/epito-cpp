#include <iostream>


int main() {
    // character output, end line
    std::cout << "hello világ" << 123 << std::endl;
    
    // def. változó
    int max;
    // character input
    std::cin >> max;

    if (max < 0) {
        std::cout << "negatív";
    } else {
        // ciklus 1. lehetőség: while
        int x = 1;
        while (x <= max) {
            std::cout << x << std::endl;
            x += 1;
        }
        // 2. lehetőség: for
        for (int y = 1; y <= max; y += 1) {
            std::cout << y << std::endl;
        }
    }
}





