#include <iostream>

// 5
// 9
// 12
// 7
// 0        végjel

// ki: 5*9*12*7

int main() {
    int szorzat = 1;

    int n;
    std::cin >> n;
    while (n != 0) {
        szorzat *= n;
        std::cin >> n;
    }
    
    // 2. lehetőség
    // while (true) {
    //     std::cin >> n;
    //     if (n == 0)
    //         break;
    //     szorzat *= n;
    // }
    
    std::cout << szorzat;
    
    return 0;
}
