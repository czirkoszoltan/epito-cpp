#include <iostream>

// function overload
int max(int a, int b) {
    if (a > b)
        return a;
    else
        return b;
}

double max(double a, double b) {
    if (a > b)
        return a;
    else
        return b;
}


int negyzet(int x) {
    return x*x;
}

// ez is overload, ezt fogja megtalálni double esetén,
// viszont az = delete miatt nem engedi lefordítani a programot
void negyzet(double x) = delete;

int main() {
    // fordítási hiba, negyzet(double)
    //~ std::cout << negyzet(2.3);
    
    std::cout << negyzet(5);
    
    std::cout << max(7, 5);     // max(int a, int b)
    std::cout << max(2.3, 1.7); // max(double a, double b)
    // std::cout << max(2, 3.4); // ambiguous
    
    
    // int i = 2.3;    // engedi
    // int j{2.3};     // nem engedi, nagyon szigorú a konverziókkal
    
    // unsigned int k{5};
    // unsigned int l{-3};    // nem engedi
    
    double d = 3.4;
    int i = (int)d;           // engedi, mert int i = int
                              // jelezzük magunknak is, hogy szándékos
}
