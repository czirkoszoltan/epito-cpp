OB#include <iostream>
#include <string>

int hol_van(std::string s, char b) {
    for (unsigned i = 0; i < s.size(); i += 1)
        if (s[i] == b)
            return i;    // nem fut tovább se a ciklus, se a függvény!

    return -1;
}

int main() {
    std::string szoveg = "hello vilag";
    
    int idx = hol_van(szoveg, 'o');
    if (idx == -1)
        std::cout << "Nincs";
    else
        std::cout << idx << ". helyen van";
}
