
/*
 * Vissza: első szóköz pozíciója
 * Nem kaphat olyan sztringet, amiben nincs szóköz
 */
int elso_szokoz(std::string str) {
    int i = 0;
    while (str[i] != ' ')
        i++;
    return i;
}

// nincs mit kommentelni, a változónevek elmondják
std::string str = "alma barack cseresznye";
int utolsobetu = elso_szokoz(str) - 1;
