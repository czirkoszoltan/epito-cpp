#include <iostream>
#include <cstdint>

// a = a + b   ->   a += b
// a = a - b   ->   a -= b
// a = a + 1
// a += 1
// a++,   a--

// a -= 1


int main() {
    int max;    
    std::cin >> max;
    
    uint64_t szorzat = 1;      // 64 bit
    
    for (int i = 2; i <= max; i++) {
        szorzat *= i;
        std::cout << szorzat << std::endl;
    }
    
    std::cout << szorzat;
    
    // konverziók
    std::cout << 5 / 2;     // int/int -> int
    std::cout << 5.0 / 2;   // double/int -> double
    int a = 5, b = 2;
    std::cout << a / b;     // int/int -> int
    std::cout << a / (double)b;   // int/double -> double
    
    int x = 2.3;            // int x = 2;
    double c = 1;           // 1=int   int->double
    double d = x;           // x=int   int->double, 2 :(
    double e = 3/4;
    std::cout << e;         // e = 0
    
    return 0;
}

