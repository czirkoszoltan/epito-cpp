#include <string>
#include "mydialog.h"

// trolltech
// qt
// cutie

MyDialog::MyDialog() {
    tag1 = new QLineEdit;
    tag2 = new QLineEdit;
    osszeg = new QLabel;
    osszeg->setText("Itt lesz az összeg");
    
    auto *kiszamit_gomb = new QPushButton("&Kiszámít");

    auto *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tag1);
    mainLayout->addWidget(tag2);
    mainLayout->addWidget(osszeg);
    mainLayout->addWidget(kiszamit_gomb);
    
    setLayout(mainLayout);
    
    connect(kiszamit_gomb, SIGNAL(clicked()), this, SLOT(kiszamit_clicked()));
}


//~ QLineEdit *tag1;
//~ QLineEdit *tag2;
//~ QLabel *osszeg;

void MyDialog::kiszamit_clicked() {
    //~ QString szoveg1 = tag1->text();
    //~ int szam1 = szoveg1.toInt();

    bool ok1, ok2;
    int szam1 = tag1->text().toInt(&ok1);
    int szam2 = tag2->text().toInt(&ok2);
    
    if (!ok1 || !ok2) {
        osszeg->setText("hiba");
        return;
    }
    
    int ossz = szam1 + szam2;
    
    //~ std::string eredmeny = std::to_string(ossz);
    //~ osszeg->setText(eredmeny.c_str());
    osszeg->setText(std::to_string(ossz).c_str());
}
