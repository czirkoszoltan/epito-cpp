#include <QtWidgets>

class MyDialog : public QDialog  {
    Q_OBJECT
    
    public:
        MyDialog();
    
    private:
        QLineEdit *tag1;
        QLineEdit *tag2;
        QLabel *osszeg;
    
    // signals & slots
    public slots:
        void kiszamit_clicked();
};
