#include "mydialog.h"

// https://doc.qt.io/qt-5/qtwidgets-layouts-basiclayouts-example.html

int main(int argc, char **argv) {

    QApplication app (argc, argv);
    
    MyDialog dialog;
    dialog.show();

    return app.exec();

}

