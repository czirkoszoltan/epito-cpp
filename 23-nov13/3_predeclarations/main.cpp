#include <iostream>

// sokszog.h

class Pont;     // elődekaráció   predeclaration

class Sokszog {
  public:
    Pont *pontok;
    int csucsok;
    
    Sokszog(int cs);
    
    int get_csucsok() const {
        return csucsok;
    }
  private:
    void uj_csucs(Pont p);
};

// pont.h

class Pont {
  public:
    int x, y;
};

// sokszog.cpp

#include "pont.h"

Sokszog::Sokszog(int cs) {
    csucsok = cs;
    pontok = new Pont[cs];
}

void Sokszog::uj_csucs(Pont p) {
    // átméretez
}



int main() {
    Sokszog h(6);
}
