#include <QDialog>

// érdemes csak elődeklarációkat használni,
// mert NAGYON gyorsítja a fordítási időt
QT_BEGIN_NAMESPACE
    class QLineEdit;
    class QLabel;
QT_END_NAMESPACE

class MyDialog : public QDialog {
    Q_OBJECT
    
    public:
        MyDialog();
    
    private:
        QLineEdit *tag1;
        QLineEdit *tag2;
        QLabel *osszeg;
    
    public slots:
        void kiszamit_clicked();
};
