#include <string>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QString>

#include "mydialog.h"

MyDialog::MyDialog() {
    tag1 = new QLineEdit;
    tag2 = new QLineEdit;
    osszeg = new QLabel;
    osszeg->setText("Itt lesz az összeg");
    
    auto *kiszamit_gomb = new QPushButton("&Kiszámít");

    auto *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tag1);
    mainLayout->addWidget(tag2);
    mainLayout->addWidget(osszeg);
    mainLayout->addWidget(kiszamit_gomb);
    
    this->setLayout(mainLayout);
    
    connect(kiszamit_gomb, SIGNAL(clicked()), this, SLOT(kiszamit_clicked()));
}


void MyDialog::kiszamit_clicked() {
    QString szoveg1 = tag1->text();
    int szam1 = szoveg1.toInt();
    int szam2 = tag2->text().toInt();
    
    int ossz = szam1 + szam2;
    
    std::string eredmeny = std::to_string(ossz);
    osszeg->setText(eredmeny.c_str());
}
