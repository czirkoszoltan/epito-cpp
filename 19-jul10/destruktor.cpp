#include <iostream>
#include <cstring>
#include <vector>

class Kiabal {
  public:
    Kiabal() {
        std::cout << "Kiabal ctor\n";
    }
    ~Kiabal() {
        std::cout << "Kiabal dtor\n";
    }
};

class Csomagolo {
    Kiabal k;
    int i;
    std::string s;
    
    // nem kell destruktort írni, viszont VAN NEKI!
    // a fordító generál olyat, ami a kiabálnak
    // és a sztringnek a destruktorát hívja.
};


void fv() {
    // látványos eredmény: 3x kiabál konstruktor,
    // de amúgy 3x sztring konstruktor is hívódik
    std::vector<Csomagolo> v(3);
    
    // látványos eredmény: 3x kiabál destruktor,
    // de amúgy 3x sztring destruktor is hívódik
}


int main() {
    fv();
}
