#include "mydialog.h"

MyDialog::MyDialog() {
    ui.setupUi(this);
    
    // első lehetséges megoldás: a tartalmazott objektumon belül
    // érhetőek el az egyes widget-ek.
    connect(ui.pushButton, SIGNAL(clicked()), this, SLOT(plus_one()));
    
}

void MyDialog::plus_one() {
    sz.novel();
    
    // első lehetséges megoldás: a tartalmazott objektumon belül
    // érhetőek el az egyes widget-ek.
    ui.label->setText(std::to_string(sz.get()).c_str());
}
