#include <QApplication>

#include "mydialog.h"

int main(int argc, char **argv) {
    QApplication app (argc, argv);
    
    MyDialog md;
    md.show();
    
    return app.exec();
}

