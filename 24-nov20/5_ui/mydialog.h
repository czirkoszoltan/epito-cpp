#include "ui_mydialog.h"

class Szamlalo {
    private:
        int szamlalo = 0;
    public:
        void novel() {
            szamlalo += 1;
        }
        int get() const {
            return szamlalo;
        }
};

class MyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MyDialog();

private:
    // első lehetséges megoldás: a ui objektumból egyszerűen
    // tartalmazni kell egy példányt.
    Ui::Dialog ui;
    Szamlalo sz;        // business logic
    
public slots:
    void plus_one();
};
