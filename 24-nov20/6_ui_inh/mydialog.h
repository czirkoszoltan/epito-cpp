#include "ui_mydialog.h"

// második megoldás: öröklés a ui objektumból IS.
class MyDialog : public QDialog, private Ui::Dialog
{
    Q_OBJECT

public:
    explicit MyDialog();

private:
    int szamlalo = 0;
    
public slots:
    void plus_one();
};
