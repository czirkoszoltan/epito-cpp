#include "mydialog.h"

MyDialog::MyDialog() {
    setupUi(this);

    // második megoldás: öröklés a ui objektumból IS.
    // ilyenkor az adattagok beépülnek a sajátok közé,
    // vagyis pushButton = this->pushButton tulajdonképpen.
    connect(pushButton, SIGNAL(clicked()), this, SLOT(plus_one()));
}

void MyDialog::plus_one() {
    ++szamlalo;
    // itt pedig label = this->label.
    label->setText(std::to_string(szamlalo).c_str());
}
