#include <vector>
#include <iostream>

// range-based for loop
// for (típus változó : tároló)


int main() {
    std::vector<int> v = { 100, 200, 300 };

    for (int & i : v)       // "a v vektorban minden integer referenciája"
        i *= 2;
    
    for (int i : v)
        std::cout << i << " ";

    for (size_t idx = 0; idx < v.size(); ++idx) {
        int & i = v[idx];
        
        i *= 2;
    }
}
