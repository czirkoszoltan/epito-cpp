#include <vector>
#include <string>
#include <iostream>

// ez inkább kompozíció (adattagokként tartalmazva), mintsem öröklés.
// nem öröklés jellegű közöttük a kapcsolat (minden micsoda micsoda, pl. minden gólya madár),
// hanem részegységek, tartalmazása (van valamilyen, pl. autónak van kereke, motorja).

class BellmanFord {
    // ...
};

class MonteCarlo {
    // ...
};

class BellmanFordMonteCarloval {
    BellmanFord bf;
    MonteCarlo mc;
};



