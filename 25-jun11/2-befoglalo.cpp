#include <vector>
#include <string>
#include <iostream>

class Kor {
    public:
        int x, y;
        int r;
};

class BefoglaloDoboz {
    public:
        int sz, m;
        
        // konstruktor: fv, objektum létrehozása
        // feladat: adattagok inicializálása
        BefoglaloDoboz(Kor k)
            : sz(k.r * 2), m(k.r * 2)       // inicializáló lista
        {
            //~ sz = k.r * 2;
            //~ m = k.r * 2;
        }
};

//~ BefoglaloDoboz nemjo;           // létrehozás: nem jó így, mert a befoglaló doboznak nincs paraméter nélküli konstruktora
//~ nemjo = BefoglaloDoboz(k1);     // ez utólagos módosítás lenne

//~ BefoglaloDoboz jo(k1);          // így viszont jó, mert megmondjuk, melyik kör alapján jöjjön létre

class BefoglaloDobozPozicioval1 {
    public:
        int x, y;
        BefoglaloDoboz bf;
        
        BefoglaloDobozPozicioval1(Kor k)
            : x(k.x) , y(k.y) , bf(k)
        {
            //~ this->bf = BefoglaloDoboz(k);
            //~ this->x = k.kx;
            //~ this->y = k.ky;
        }
};

class BefoglaloDobozPozicioval2 : public BefoglaloDoboz {
    public:
        // int sz, m; -> örökölve
        int x, y;
        
        BefoglaloDobozPozicioval2(Kor k)
            // 1. ősosztály konstruktorának paraméterezése
            // 2. adattagok inicializálása
            : BefoglaloDoboz(k), x(k.x) , y(k.y)
        {
            //~ this->bf = BefoglaloDoboz(k);
            //~ this->x = k.kx;
            //~ this->y = k.ky;
        }
};




int main() {
    std::vector<Kor> korok;

    std::vector<BefoglaloDoboz> befoglalok;
    for (Kor const & k : korok)
        befoglalok.push_back(BefoglaloDoboz(k));
}
