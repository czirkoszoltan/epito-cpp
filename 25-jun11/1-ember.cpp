#include <vector>
#include <string>
#include <iostream>

class Ember {
    public:
        std::string nev;

        // van konstruktora -> a fordító nem csinál alapértelmezett konstruktort
        // (nulla paraméterűt). ez így azt jelenti, hogy nem lehet embert létrehozni
        // név nélkül. vagyis ez nem jó:
        //    Ember e;
        // ez viszont jó:
        //    Ember e("Mézga Géza");
        Ember(std::string pnev)
            : nev(pnev)     // az adattag "név" sztring konstruktorát ezzel lehet paraméterezni
        {
            std::cout << nev.length();      // a név hossza: oké, mert ilyenkorra a "név" adattag létrejött
        }
};

class Tanulo : public Ember {
    public:
        std::vector<int> jegyek;
        
        Tanulo(std::string pnev)
            : Ember(pnev)   // az ősosztály konstruktorát így lehet paraméterezni
           /* , jegyek()   -> üres vektor automatikusan */
        {
        }
};
