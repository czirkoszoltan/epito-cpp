#include "tort.h"

// :: scope operator
// definíciónál meg kell adni, melyik osztályhoz tartozik a függvény!
Tort::Tort(int sz, int n) {
    int a = lnko(sz, n);
    szaml = sz / a;
    nev = n / a;
}

int Tort::lnko(int a, int b) {
    while (b != 0) {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}


// itt nincs Tort::, de ez globális függvény (!)
Tort operator+ (Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_nev() + b.get_szaml() * a.get_nev(),
        a.get_nev() * b.get_nev()
    );
}

// ez is globális
std::ostream & operator<< (std::ostream & os, Tort t) {
    os << t.get_szaml() << '/' << t.get_nev();
    return os;
}
