#include <iostream>
#include "tort.h"

int main() {
    Tort a(5, 10);
    Tort b(1, 4);
    std::cout << a << " + " << b << " = " << a+b << std::endl;
}
