#include <iostream>

// a típusok (osztályok) a fejlécfájlba mennek
class Tort {
  public:
    // függvény deklarációja
    Tort(int sz, int n);
    
    // definiálhatom itt egyből, ez ún. inline
    // függvénydefiníció
    int get_szaml() const {
        return szaml;
    }
    int get_nev() const {
        return nev;
    }

  private:
    int szaml;
    int nev;
    int lnko(int a, int b);     // deklaráció
};

// globális függvények deklarációi
Tort operator+ (Tort a, Tort b);
std::ostream & operator<< (std::ostream & os, Tort t);
