#include <iostream>
#include <string>

namespace elso {
    void random() {
        std::cout << "elso nevter random fv\n";
    }
}

namespace masodik {
    void random() {
        std::cout << "masodik nevter random fv\n";
    }
}

int main() {
    using std::cout;        // std::cout -> cout rövid néven
    
    elso::random();
    masodik::random();
    std::cout << random() << "\n";
    cout << random() << "\n";       // cout -> std::cout
    
    std::string s;
    s = "hello";
    s += "!";
}

