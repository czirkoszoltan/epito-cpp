#include <iostream>

void hossz(std::string const & s) {
    std::cout << s << " " << s.length() << std::endl;
}

int main() {
    // unicode kódolás: egy karaktert több bájt
    // reprezentál. az std::string erről sajnos
    // nem vesz tudomást, a bájtok számát adja.
    hossz("hello");
    hossz("helló");
    hossz("o");
    hossz("☺");
    hossz("🙏");
    hossz("😻");
}
