// copy paste ide a felez.h nevű fájl:
// így láta a deklarációkat, össze tudja hasonlítani
// az itteni definíciókkal
#include "felez.h"

// függvénydefiníció: a függvény törzse
double fele(double x) {
    return x / 2;
}

// globáls változó definíciója
int glob = 3;
