#include <iostream>
#include <vector>

// sablon osztály

template <typename T1, typename T2>
struct Pair {        // std::pair
    T1 first;
    T2 second;
};



template <typename TYPE>
class Vektor {
    private:
        size_t meret;
        TYPE * adat;      // new double[méret]  ->  double *
    
    public:
        explicit Vektor(size_t kezdmeret) {
            meret = kezdmeret;
            adat = new TYPE[meret] ();            // 0-val inic
        }
        ~Vektor() {
            delete[] adat;
        }
        Vektor (Vektor const & masolando) {
            meret = masolando.meret;
            adat = new TYPE[meret];
            for (size_t i = 0; i < meret; ++i)
                adat[i] = masolando.adat[i];
        }
        Vektor & operator= (Vektor const & masolando) {
            if (this != &masolando) {
                delete[] adat;
                meret = masolando.meret;
                adat = new TYPE[meret];
                for (size_t i = 0; i < meret; ++i)
                    adat[i] = masolando.adat[i];
            }
            return *this;
        }
        
        size_t get_meret() const { return meret; }
        
        TYPE & operator[] (size_t idx) {
            return adat[idx];
        }
        
        void resize(size_t ujmeret) {
            TYPE * ujadat = new TYPE[ujmeret] ();
            size_t kisebb = meret < ujmeret ? meret : ujmeret;
            for (size_t i = 0; i < kisebb; ++i)
                ujadat[i] = adat[i];
            delete[] adat;
            adat = ujadat;
            meret = ujmeret;
        }
};


int main() {
    
    Vektor<double> v(20);
    
    
    v[12] = 3.14;
    for (size_t i = 0; i < v.get_meret(); ++i)
        std::cout << v[i] << " ";
    std::cout << std::endl;
    
    Vektor<int> v2(100);
    v2[5] = 123;
    
    Vektor<String> v3(50);
    v3[7] = "alma";
}
