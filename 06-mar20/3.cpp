#include <iostream>
#include <algorithm>

// sablon függvének


template <typename TYPE>
void masol(TYPE *forras, int meret, TYPE *cel) {
    for (int i = 0; i < meret; ++i)
        cel[i] = forras[i];
}

template <typename TYPE>
TYPE kisebb(TYPE a, TYPE b) {
    if (a < b)
        return a;
    else
        return b;
}


template <typename TYPE>
TYPE legkisebb(TYPE *tomb, int meret) {     // TYPE*  ~  int*   -> TYPE=int
    TYPE min = tomb[0];
    for (int i = 1; i < meret; ++i)
        if (tomb[i] < min)
            min = tomb[i];
    return min;
}

template <typename TYPE>
TYPE legkisebb(Vektor<TYPE> const & tomb) {     // Vektor<double> ~ Vektor<??>      TYPE=double
    TYPE min = tomb[0];
    for (int i = 1; i < tomb.get_meret(); ++i)
        if (tomb[i] < min)
            min = tomb[i];
    return min;
}


int main() {
    Vector<double> v(100);
    std::cout << legkisebb(v);
    
    
    
    for (int i = 0; i < 8; ++i)
        f(i);
    
    
}
