#include <iostream>
#include <cstring>

// sztring osztály. most már teljesen kész.
class String {
    private:
        size_t hossz;
        char *szoveg;
    public:
        // konstruktor. az alapértelmezett paraméterérték
        // miatt default konstruktor is.
        String(char const *kezdeti = "") {
            std::cout << "sima ctor " << kezdeti << "\n";
            // mennyi a sztring hossza? annyi +1 méretű tömb kell
            // (a lezáró \0 miatt, amit a strcpy tesz oda)
            hossz = strlen(kezdeti);
            szoveg = new char[hossz + 1];
            strcpy(szoveg, kezdeti);
        }
        
        // másoló konstruktor (copy constructor)
        // akkor hívódik, ha
        // - új objektumot inicializálok sztringgel: String s2 = s1
        // - String típusú, érték paraméter van: void f(String s);  f(...)
        String (String const & masolando) {
            std::cout << "másoló konstruktor " << masolando.szoveg << "\n";
            hossz = masolando.hossz;
            szoveg = new char[hossz + 1];
            strcpy(szoveg, masolando.szoveg);
        }
        
        // értékadó operátor, akkor hívódik, ha MEGLÉVŐ
        // sztringek közti értékadást csinálok
        // a = b     -> return a
        // a = a;
        String &  operator= (String const & masolando) {
            std::cout << "értékadás\n";
            if (this != &masolando) {
                delete[] szoveg;
                hossz = masolando.hossz;
                szoveg = new char[hossz + 1];
                strcpy(szoveg, masolando.szoveg);
            }
            return *this;
        }
        
        ~String() {     
            // destruktor, automatikusan fut, amikor megszűnik az objektum.
            // itt a konstruktorban lévő dinamikusan foglalt new char[] párja!
            std::cout << "destruktor\n";
            delete[] szoveg;
        }
        
        char & operator[] (size_t idx) {
            return szoveg[idx];
        }
        char const & operator[] (size_t idx) const {
            // overload az indexelő operátor számára. az objektum
            // konstansságára is lehet overloadolni!
            // fontos, hogy a visszatérési értékbe is const került:
            // ha a sztring konstans (jobb szélen lévő const),
            // akkor a visszaadott karakterre is úgy mutassunk rá,
            // hogy ne lehessen megváltoztatni (bal szélen const)
            return szoveg[idx];
        }
        
        size_t get_hossz() const {
            return hossz;
        }
        
        void operator+= (char c) {
            // karakter hozzáfűzése. eggyel nagyobb tömb kell,
            // régi hossz, +1 az új karakternek, +1 a lezáró nullának
            char *uj = new char[hossz + 1 + 1];
            strcpy(uj, szoveg);
            uj[hossz] = c;
            uj[hossz + 1] = '\0';
            // régi tömb nem kell; a this->szoveg pedig átállítható
            // az új tömbre.
            delete[] szoveg;
            szoveg = uj;
            hossz += 1;
        }
};

std::ostream & operator<< (std::ostream & os, String const & s) {
    for (size_t i = 0; i < s.get_hossz(); ++i)
        os << s[i];
    return os;
}


class Vonatjegy {
  public:
    String honnan = "Budapest";
    String hova = "Kecskemét";
};


int main() {
    Vonatjegy * p = new Vonatjegy[10];
    delete[] p;
}


