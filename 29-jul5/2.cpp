#include <iostream>
#include "2.h"

void MinMaxKeres::min_max_keres() {
    min_idx = 0;
    max_idx = 0;
    for (size_t i = 1; i < szamok.size(); ++i) {
        if (szamok[i] < szamok[min_idx])
            min_idx = i;
        if (szamok[i] > szamok[max_idx])
            max_idx = i;
    }
}

void MinMaxKeres::min_max_kiir() {
    std::cout << "Legkisebb: " << szamok[min_idx] << ", legnagyobb: " << szamok[max_idx] << std::endl;
}

