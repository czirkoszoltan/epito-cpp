#include <iostream>

class Complex {
    private:
        double re = 0.0, im = 0.0;
        
    public:
    
        Complex(double re, double im) {
            this->re = re;
            this->im = im;
        }
        
        // konverzió!
        Complex(double re) {            // valós -> komplex
            this->re = re;
        }
        
        Complex() = default;
};

int main() {
    double d[100] = {3.14, 12, 34, 45, 67};
    Complex c[100];
    
    for (unsigned i = 0; i < 100; ++i)
        c[i] = d[i];              // komplex <- valós
    
    (void)c;
}


