#include <iostream>

// objektumorientált programozás
// - adatrejtés
// - egységbe zárás

// típusdefíció
// osztály = egységbe zárva az adatok, és az azokon dolgozó függvények
// tanulság a törtnél: elrejtjük az adatokat (számláló és nevező),
// így van felette kontrollunk. jelen esetben a kontroll: a konstruktorban
// egyszerűsítünk. mivel csak a konstruktorral lehet törtet létrehozni,
// biztosak lehetünk benne, hogy minden tört egyszerűsítve lesz.
class Tort {
  public:   // mindenki lát, interfész
    // konstruktora
    // automatikusan fut, amikor létrejön egy objektum
    Tort(int sz, int n) {
        int a = lnko(sz, n);
        szaml = sz / a;     // szaml, nev -> relatív prím
        nev = n / a;
    }
    
    // egy paraméterű konstruktor, egész számot csinál (számláló/1)
    // int->Tort konverziót is jelent
    Tort(int sz = 0) {
        szaml = sz;
        nev = 1;
    }
    
    // 4/5 -> 0.8
    // Tort->double konverziót így kell csinálni
    // explicit = külön kérni kelljen, mert adatvesztés
    explicit operator double () const {
        return (double)szaml / (double)nev;
    }

    // getter, hogy kiolvasson adatot
    // tagfüggvénye az osztálynak, metódus, van hozzáférése a privát adattagokhoz
    // const, mert nem változtatja meg a törtet
    int get_szaml() const {
        return szaml;
    }
    int get_nev() const {
        return nev;
    }
    
  private:  // csak feljogosított függvények
    int szaml;
    int nev;
    
    int lnko(int a, int b) {
        while (b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }   // a változó -> legnagyobb közös osztó
        return a;
    }
};

// összeadás operátor
// ezt írva: t1 + t2
// a fordító ezeket keresi:
//  - operator+ (t1, t2)       tort_osszead(t1, t2)
//  - t1.operator+ (t2)        t1.osszead(t2)
// tetszőlegesen választhatunk, hogy csináljuk meg.
// itt most a globális lett (nem tagfüggvény),
// mert a getterek és a konstruktor elég a feladathoz
Tort operator+ (Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_nev() + b.get_szaml() * a.get_nev(),
        a.get_nev() * b.get_nev()
    );
}

Tort operator- (Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_nev() - b.get_szaml() * a.get_nev(),
        a.get_nev() * b.get_nev()
    );
}

Tort operator* (Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_szaml(),
        a.get_nev() * b.get_nev()
    );
}

Tort operator/ (Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_nev(),
        a.get_nev() * b.get_szaml()
    );
}

// ha van + operátor, attól még += nem lesz magától,
// meg kell külön írni. figyelni kell arra, hogy
// ez változtatja a bal oldali operandust -> referencia.
// a-t változtatja
// b-t nem akarja változtatni
Tort & operator+= (Tort & a, Tort b) {
    a = a + b;
    return a;
}

Tort & operator-= (Tort & a, Tort b) {
    a = a - b;
    return a;
}

// kiíró operátor
// return os a láncolhatóság miatt (cout << t1 << t2 << ...)
std::ostream & operator<< (std::ostream & os, Tort t) {
    os << t.get_szaml() << '/' << t.get_nev();
    return os;
}

// beolvasó operátor
// return is a láncolhatóság miatt (cin >> t1 >> t2 >> ...)
std::istream & operator>> (std::istream & is, Tort & t) {
    int sz, n;
    char c;
    is >> sz >> c >> n;
    t = Tort(sz, n);
    return is;
}

int main() {
    Tort t1(5, 10);   // 1/2
    Tort t2(1, 4);
    
    //~ std::cout << t2 << '\n';
    //~ std::cout << operator+(t1, t2);
    //~ operator<< (std::cout, operator+(t1, t2));
    std::cout << t1 + t2 << '\n';
    
    
    Tort t3(5);
    std::cout << t3 << std::endl;
    Tort t4;
    std::cout << t4 << std::endl;
    
    
    Tort t5(5, 15);
    std::cout << t5 << std::endl;
    
    // működnek, mert van int->Tort konverzió
    // az egyparaméterű konstruktor által;
    // 3 = Tort(3) stb.
    std::cout << t5 + 3 << std::endl;
    std::cout << 3 * t5 << std::endl;
    std::cout << t5 / 2 << std::endl;
    std::cout << 2 / t5 << std::endl;
    
    // a megírt operator double miatt
    std::cout << (double)t5;
}
