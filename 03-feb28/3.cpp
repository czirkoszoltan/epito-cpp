#include <iostream>

class Szamlalo {
  public:
    int kovetkezo() {
        n += 1;
        return n;
    }

  private:
    int n = 0;
};

void leptet(Szamlalo & s) {
    s.kovetkezo();
}

int main() {
    Szamlalo sz;
    Szamlalo sz2;
    
    std::cout << sz.kovetkezo();
    std::cout << sz2.kovetkezo();
    std::cout << sz.kovetkezo();
    std::cout << sz2.kovetkezo();
}
