#include <iostream>
#include <string>


// érték szerinti paraméterátadás
// paraméternek értéke/tartalma lemásolódik
void duplaz_nem_mukodik(int a) {
    // két változó van: main int x = 3    saját int a = 6
    // később saját int a = 6 -> kuka
    // a hívó változóját nem is látja, csak a másolatát!
    a = a * 2;
    std::cout << a << std::endl;
}



// 1. forma: érték szerinti
//           munkamásolat, elhasználhatja
void ertek_szerinti(std::string i) {
}

// 2. forma: konstans ref szerinti
//           nem változtat, nagy objektum, sok adat
//           vizsgáljuk a tartalmat
void kiir(const std::string & i) {
    std::cout << i;
}
// 3. forma: cím szerinti, ref szerinti
//           ha változtatni kell
void felkialt(std::string & i) {
    i += '!';
}

// referencia szerinti paraméterátadás
// cím szerinti ~
// nem másolatot kér, hanem az eredetit akarja látni
void duplaz(int & a) {
    a = a * 2;
}

int main() {
    int x = 3;

    std::cout << x << std::endl;
    duplaz(x);
    std::cout << x << std::endl;

    // duplaz(12) -> fordítási hiba, nem változó
    // const int a = 5;
    // duplaz(a); -> fordítási hiba, konstans
}
