#include <iostream>


// aliasing
// két pointer, két referencia típusa megegyezik, két változó típusa megegyezik
// -> a fordító feltételezi, hogy a pointer/referencia/változó UGYANAZ


// Pl. ez a függvénynem biztos, hogy 3-mal tér vissza,
// mert ha a hívás ilyen: f1(&x, &x), akkor a == b,
// mindkét pointer ugyanarra az egy integerre mutat,
// tehát a *b=2 is x=2-t jelent, mint ahogyan az előtte
// lévő *a=1 is x=1-et. A visszatérési érték ilyenkor 4.

int f1(int * a, int * b) {
    *a = 1;
    *b = 2;
    return *a + *b;     // 1 + 2 ?
}



// Ez sem biztos, hogy 5+3, mert ha f2(&glob2) a hívás,
// akkor *p=3 a glob2-t módosítja.

int glob2 = 1;

int f2(int *p) {
    glob2 = 5;
    *p = 3;     
    return glob2 + *p;      // 5 + 3 ?
}






// A pointer hiába konstans, olyan változóra mutat,
// ami tőle függetlenül megváltozhat!
// Mint pl. f3(&glob3) függvényhívás esetén.

int glob3 = 3;

int f3(const int *p) {
    int x1 = *p;
    //
    //~ glob3 = 3;
    //
    int x2 = *p;        // ugyanaz, mint x1?
    // Mit ad vissza?
    return x1 - x2;
}



// Nekem itt mindegyik függvénynél úgy tűnik, hogy a visszatérési értéket
// - a számítás eredményét - paraméterben adja a függvény. A visszatérési
// érték HELYE pedig előre létezik, indirekten van átadva - ezért nem tudja
// a fordító, van-e aliasing.
// A megoldás:
// - Kapja a függvény akár érték szerint PARAMÉTERKÉNT az adatot, amivel dolgozik.
//   Ha érték szerint kapja, akkor tudja a fordító, hogy nincs aliasing.
//   Ha túl sok másolni, akkor legyen referencia, de a következő pont akkor is fontos.
// - Hozza a függvény létre a visszatérési értéket. Így tudja majd a
//   fordító, hogy nincs aliasing.



struct Eredmeny {
    vector<int> a_tomb;      // a.data    a.size
    vector<int> b_tomb;
};


extern Eredmeny TA_NO_STR_1st_Bellman_Levente1(std::vector<int> const& bemenoadat, const int start_act, const int N, const int M) {
    Eredmeny er;
    
    er.a_tomb.resize(M * N);
    er.b_tomb.resize(N - 2);
    
    return er;      // named return value optimization  -> ingyen van, nem másolódik 'er'
}


Eredmeny eredmeny = TA_NO_STR_1st_Bellman_Levente1(.......);

eredmeny.a_tomb.size()
