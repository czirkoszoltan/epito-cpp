/*
 * VECTOR
 */


	int TA_NO_STR_1st_Bellman_Levente2_v(const int start_act, const int N, const int M, vector<int>& lista)
	{
        
         vissza      [7 5 9 8 1 ]
         munka       [ 4 3 2 ]
                    
            
        //~ 0..szame       értéktelen adat 
        //~ szame..szamv   értékes

        auto talalat = std::find(lista.begin(), lista.end(), valt);
        if (talalat != lista.end()) {
            munka.erase(talalat);
            vissza.push_back(valt);         // O(1)
        }
                        
                        
                    
/*
 * Az erase és az insert nagyon kellemetlen művelet a vektornak,
 * mert a beszúrás / törlés helyétől a vektor végéig egyszerre
 * az összes adatot meg kell mozgatni. Ha pár elem van, ez teljesen
 * oké, de pár tíz-száz fölött kezd gáz lenni.
 * 
 * - Számít az elemek sorrendje, amik itt előállnak? Ha nem, más
 *   adatszerkezet jó lehet.
 * - Ha igen, akkor is lehet, közben érdemes másik tárolót építeni
 *   a már eltárolt elem újbóli megtalálásához.
 * - Jó ötlet lehet mindent eltárolni, és utólag kiválogatni
 *   a különbözőket (std::sort, std::unique).
 * - Jó ötlet lehet mindig a végén módosítani a vektort:
 *   insert(begin, x) helyett push_back.
 */
