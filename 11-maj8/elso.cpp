/*
 * REFERENCIÁK
 */

void TA_NO_STR_1st_Bellman_Levente1(const int start_act, const int N, const int M, vector<int>& lista)//Start_Act, NumOfAct, NumOfLinks

    if (actres[linkdata[temp_rel].FromNode].ES + linkprep[temp_rel].SSE > actres[linkdata[temp_rel].ToNode].ES)
    {
        actres[linkdata[temp_rel].ToNode].ES = actres[linkdata[temp_rel].FromNode].ES + linkprep[temp_rel].SSE;
        
// common subexpression elimination

xyz = actres[linkdata[temp_rel].FromNode].ES;

for (int i = 0...     1000) {
    abc = 123;
    ab.push_back(34);
}

// HELYETTE:

    act_resdata & fromnode = actres[linkdata[temp_rel].FromNode];
    act_resdata & tonode = actres[linkdata[temp_rel].ToNode];
    auto & sse = linkprep[temp_rel].SSE;

    if (fromnode.ES + sse > tonode.ES)
    {
        tonode.ES = fromnode.ES + sse;


debug mode   -> hibakeresés
release      -> optimalizálás






/*
 * SCOPE
 */


	void TA_NO_STR_1st_Bellman_Levente2(const int start_act, const int N, const int M)
	{
		int *listaptr;

        // .... 30 sorral később .....

					listaptr = std::find(lista2 +szame, lista2 + szamv, linkdata[temp_rel].ToNode);

    // HELYETTE:

	void TA_NO_STR_1st_Bellman_Levente2(const int start_act, const int N, const int M)
	{
        // .... 30 sor .....

					int *listaptr = std::find(lista2 +szame, lista2 + szamv, linkdata[temp_rel].ToNode);


/*
 * Változót mindig csak akkor létrehozni, amikor először szükség van rá;
 * amikor először értéket is tud kapni.
 * Minél kisebb legyen a scope-ja, minél kisebb blokkban jöjjön létre és
 * szűnjön is meg!
 */













/*
 * MINDENHOL
 */

    while ((szamv - szame++ != 0) && (szamv < szorzat))
    {
        // ...
    }

/*
 * Valójában ezeket nem értem, mit csinálnak; ne legyen vizsgáló
 * kifejezésben mellékhatás (száme++). Az kerüljön máshova.
 * "command-query separation principle":
 * - vagy kérdés, de akkor ne legyen mellékhatás,
 * - vagy módosító művelet, de akkor nem használjuk az értéket.
 *
 * A ciklus így: */

    int szame = 0;
    while ((szame < szame) && (szamv < szorzat))
    {
        // ...
        szame ++;
    }
