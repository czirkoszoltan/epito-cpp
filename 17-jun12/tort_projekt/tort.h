#ifndef TORT_H_INCLUDED
#define TORT_H_INCLUDED

class Tort {
    public:
        Tort(int sz, int n);
    
    private:
        int sz_;
        int n_;
};

#endif
