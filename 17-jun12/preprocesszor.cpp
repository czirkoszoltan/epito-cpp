#include <iostream>

class Complex {
    private:
        double re_, im_;
        
        double egyik_;
        double _masik;
        double m_harmadik;      // member
        
        
    public:
        Complex(double re, double im) {
            this->re_ = re;
            this->im_ = im;
            
        }
        
        double re() const {
            return re_;
        }
        double im() const {
            return im_;
        }
};


// előre definiált preprocesszor makrók
int main() {
    std::cout << __FILE__ << std::endl;
    std::cout << __LINE__ << std::endl;
    std::cout << __DATE__ << std::endl;
    std::cout << __TIME__ << std::endl;
}




