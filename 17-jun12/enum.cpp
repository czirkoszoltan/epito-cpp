#include <iostream>

// nem túl jó megoldás: fejben tartott értékek
// 0 = treff
// 1 = kőr

// sokkal jobb megoldás: felsorolt típus
enum KartyaSzin {
    treff,      // 0
    kor,        // 1
    pikk,       // 2
    karo,       // 3
};

struct Kartya {
    KartyaSzin szin;
};


int main() {
    KartyaSzin szin;
    szin = pikk;
    if (szin == kor)
        std::cout << "...";
    
    //~ uint8_t szin;
    //~ szin = 2;
    //~ if (szin == 1)
}
