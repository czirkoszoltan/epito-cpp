#include <algorithm>
#include <iterator>
#include <iostream>


 
int main() {
    int arr[] = { 7, -4, 8, -3, -7, 9, -3, 2, 4 };
                                
    // c++1998: sort, iterátorok
    // c++2011: lambda
    std::sort(std::begin(arr), std::end(arr), [] (int x, int y) {
        return x > y;
    });
    
    for (auto i : arr)
        std::cout << i << " ";
    std::cout << std::endl;

    int c = std::count_if(std::begin(arr), std::end(arr),
        [] (int i) {
            return i < 0;
        }
    );
    std::cout << c << " db" << std::endl;
}
