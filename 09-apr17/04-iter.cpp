#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

// minden elemet kiír. a két iterátor közti
// tartományból. [eleje; vége), mindig balról
// zárt, jobbról nyílt intervallum
// iterátor: pointer interfésszel rendelkező objektum
// egy tároló elemeinek eléréséhez,
// *it: adat, ++it: ugrás a következőre
template <typename ITER>
void mindet_kiir(ITER eleje, ITER vege) {
    for (ITER it = eleje; it != vege; ++it)
        std::cout << *it << ", ";
    std::cout << std::endl;
}

template <typename ITER, typename FUNC>
void mindegyiket_feldolgoz(ITER eleje, ITER vege, FUNC f) {
    for (ITER it = eleje; it != vege; ++it)
        f(*it);
}

 
int main() {
    int t[5] = { 8, 9, 15, 6, 3 };
    mindet_kiir(std::begin(t), std::end(t));      // ITER = int*
    
    mindegyiket_feldolgoz(std::begin(t), std::end(t),
        [] (int i) { std::cout << i << ','; }
    );
    std::cout << std::endl;

    // int *p = t    legelső elem
    // p != t + 5    utolsó utáni!!! elem
    // [t, t + 5)    balról zárt, jobbról nyílt
    
    std::vector<int> v = { 9, 12, 3, 5, 7 };
    
    // auto it = v.begin();
    // ^^^^
    // std::vector<int>::iterator
    
    mindet_kiir(v.begin(), v.end());
    
    std::set<std::string> s = { "alma", "körte", "alma", "barack", "barack" };
    mindet_kiir(s.begin(), s.end());
    
    std::list<double> l = { 9, 12, 3, 5, 7  };
    mindet_kiir(l.begin(), l.end());
}
