#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>

class Page {
  public:
    explicit Page(int w, int h) : w_(w), h_(h), page_(w*h) {
        clear();
    }

    void clear() {
        std::fill(page_.begin(), page_.end(), ' ');
    }

    void print() const {
        for (int y = 0; y < h_; ++y) {
            for (int x = 0; x < w_; ++x)
                std::cout << page_[y*w_ + x];
            std::cout << std::endl;
        }
    }

    void setchar(int x, int y, char c) {
        if (x >= 0 && x < w_ && y >= 0 && y < h_)
           page_[y*w_ + x] = c;
    }

    int get_width() const {
        return w_;
    }

    int get_height() const {
        return h_;
    }

  private:
    int w_, h_;
    std::vector<char> page_;
};

// Egyenes és parabola: egyforma interfészek!
class Egyenes {
  private:
    double a = -0.5, b = 0.3;

  public:
    double operator() (double x) {
        return a*x + b;
    }
};

class Parabola {
  private:
    double a, b, c;

  public:
    Parabola(double a_, double b_, double c_): a(a_), b(b_), c(c_) {}
    double operator() (double x) {
        return a*x*x + b*x + c;
    }
};

// sin(d*x)
// kicsit kényelmetlen, hogy emiatt egy osztályt kell
// írni... erre jó a lambda.
class SinDx {
  private:
    double d;
  public:
    SinDx(double d_) : d(d_) {}
    double operator() (double x) {
        return sin(d*x);
    }
};

// template metaprogramming

template <typename FV>
void plot(Page &p, char c, FV f) {
    for (int x = 0; x < p.get_width(); ++x) {
        double fx = (x - p.get_width()/2.0)/4.0;
        double fy = f(fx);
        int y = -(fy * 4.0) + p.get_height()/2;
        p.setchar(x, y, c);
    }
}
 
int main() {
    Page oldal(78, 18);
    
    plot(oldal, 'x', Parabola(0.2, 0.4, -0.7));
    
    double d = 0.8;
    plot(oldal, '#',        // [d]   lambda introducer,   capture specifier
        [d] (double x) {
            return sin(d * x);
        }
    );
    //~ Egyenes e;
    //~ plot(oldal, e, '#');        // plot<Egyenes>  (oldal, e, '#');
 
    double a = 0.1;
    plot(oldal, '*', [a] (double x) { return a*x*x; });

    oldal.print();
}

