#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>

class Page {
  public:
    explicit Page(int w, int h) : w_(w), h_(h), page_(w*h) {
        clear();
    }

    void clear() {
        std::fill(page_.begin(), page_.end(), ' ');
    }

    void print() const {
        for (int y = 0; y < h_; ++y) {
            for (int x = 0; x < w_; ++x)
                std::cout << page_[y*w_ + x];
            std::cout << std::endl;
        }
    }

    void setchar(int x, int y, char c) {
        if (x >= 0 && x < w_ && y >= 0 && y < h_)
           page_[y*w_ + x] = c;
    }

    int get_width() const {
        return w_;
    }

    int get_height() const {
        return h_;
    }

  private:
    int w_, h_;
    std::vector<char> page_;
};
 
//  double -> double
//  double (*pfv)(double);

//  y = f(x)

// Függvényre mutató pointer: melyik egyváltozós
// matematikai függvényt rajzolja ki.
void plot(Page &p, double (*pfv)(double), char c) {
    for (int x = 0; x < p.get_width(); ++x) {
        double fx = (x - p.get_width()/2.0)/4.0;
        double fy = pfv(fx);
        int y = -(fy * 4.0) + p.get_height()/2;
        p.setchar(x, y, c);
    }
}

// Probléma: hogy juttatunk el a függvényhez
// további adatokat, pl. a parabola együtthatóit?
// A globális változó működik, de szebb lenne összekötni
// őket a függvénnyel valahogy.
double a = 0.5, b = 0.3, c = -0.2;

double parabola(double x) {
    return a*x*x + b*x + c;
}
 
int main() {
    Page p(78, 18);
    
    plot(p, parabola, 'x');
 
    p.print();
}
