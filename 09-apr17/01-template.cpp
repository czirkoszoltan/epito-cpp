template <typename T>
void swap(T & a, T & b) {
    T temp = a;
    a = b;
    b = temp;
}

// Fordító képes a fentiből generálni:
// template <>
// void swap<int>(int & a, int & b) {
//     int temp = a;
//     a = b;
//     b = temp;
// }
// 
// template <>
// void swap<double>(double & a, double & b) {
//     double temp = a;
//     a = b;
//     b = temp;
// }


// Template paraméterek:
// - típus is lehet (typename)
// - nem típus (pl. egész szám)
// Ugyanaz a szintaxis, mint a normál
// függvényeknél: típus név, típus név, típus név...
template <typename T, int N>
void f() {
    T tomb[N];
}


int main() {
    f<double, 20> ();
    f<char, 20> ();
    
    int i, j;
    swap(i, j);     // swap<int>
    double x, y;
    swap(x, y);     // swap<double>
}
