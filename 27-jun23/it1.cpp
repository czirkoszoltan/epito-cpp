#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

// T helyére bármilyen típus kerülhet
// duck typing - ha van >, akkor jó lesz.
template <typename T>
T max(T a, T b) {
    if (a > b)
        return a;
    return b;
}

// kiír minden elemet a tartományban.
// eleje -> balról zárt; vége -> jobb nyílt (dijkstra pdf!)
template <typename T>
void kiir(T eleje, T vege) {
    for (T p = eleje; p != vege; ++p)
        std::cout << *p << " ";
    std::cout << "\n";
}

template <typename T, typename PRED>
std::vector<T> filter(std::vector<T> const & v, PRED f) {
    std::vector<T> res;
    for (auto const & elem : v)     // for (auto it = v.begin().....)
        if (f(elem))
            res.push_back(elem);
    return res;
}

template <typename T, typename F>
std::vector<T> map(std::vector<T> const & v, F f) {
    std::vector<T> res;
    for (auto const & elem : v)     // for (auto it = v.begin().....)
        res.push_back(f(elem));
    return res;
}


template <typename T>
bool paros_e(T i) {
    return i % 2 == 0;
}

int vektorban_parosak(std::vector<int> const & v) {
    return std::count_if(v.begin(), v.end(), paros_e<int>);
}



template <typename T>
bool pozitiv_e(T i) {
    return i > 0;
}


int main() {
    std::vector<int> t = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    kiir(t.begin(), t.end());       // std::vector<double>::iterator    std::vector<double>::iterator
                                    // T = std::vector<double>::iterator

    std::set<double> h = { 1, 2, 2, 2, 2, 2, 1, 1, 1, 3, 3 };
    kiir(h.begin(), h.end());       // T = std::set<int>::iterator
    
    std::cout << std::count_if(t.begin(), t.end(), paros_e<int>) << std::endl;
    std::cout << std::count_if(h.begin(), h.end(), pozitiv_e<double>) << std::endl;
    
    int oszto;
    std::cout << "osztó? ";
    std::cin >> oszto;

    // lambda kifejezés   -> lambda expression
    std::vector<int> oszthatok = filter(t, [oszto] (int i) {
        return i % oszto == 0;
    });
    
    kiir(oszthatok.begin(), oszthatok.end());
    std::vector<int> negativan = map(oszthatok, [] (int i) { return -i; } );
    kiir(negativan.begin(), negativan.end());
}
