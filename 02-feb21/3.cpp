#include <iostream>


// objektumorientált programozás
// - adatrejtés
// - egységbe zárás

// típusdefíció
// osztály = egységbe zárva az adatok, és az azokon dolgozó függvények
// tanulság a törtnél: elrejtjük az adatokat (számláló és nevező),
// így van felette kontrollunk. jelen esetben a kontroll: a konstruktorban
// egyszerűsítünk. mivel csak a konstruktorral lehet törtet létrehozni,
// biztosak lehetünk benne, hogy minden tört egyszerűsítve lesz.
class Tort {
  public:   // mindenki lát, interfész
    // konstruktora
    // automatikusan fut, amikor létrejön egy objektum
    Tort(int sz, int n) {
        int a = lnko(sz, n);
        szaml = sz / a;     // szaml, nev -> relatív prím
        nev = n / a;
    }

    // getter, hogy kiolvasson adatot
    // tagfüggvénye az osztálynak, metódus, van hozzáférése a privát adattagokhoz
    // const, mert nem változtatja meg a törtet
    int get_szaml() const {
        return szaml;
    }
    int get_nev() const {
        return nev;
    }
  
  private:  // csak feljogosított függvények
    int szaml;
    int nev;
    
    int lnko(int a, int b) {
        while (b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }   // a változó -> legnagyobb közös osztó
        return a;
    }
};

void kiir_tort(Tort a) {
    std::cout << a.get_szaml() << '/' << a.get_nev() << std::endl;
}

Tort osszead_tort(Tort a, Tort b) {
    return Tort(
        a.get_szaml() * b.get_nev() + b.get_szaml() * a.get_nev(),
        a.get_nev() * b.get_nev()
    );
}

int main() {
    // objektum
    
    Tort t1(5, 10);   // 1/2    Tort összetett típus
    kiir_tort( Tort(50, 200) );     // konstruktor fv. hívás formájában
                                    // temporális objektum, ideiglenes
                                    // pontosvesszőig tart az élettartama

    Tort t2(1, 4);
    
    //~ std::cout << t1 + t2;
    kiir_tort(osszead_tort(t1, t2));
    
    //~ Tort t2 = t1;
    //~ Tort tomb[30];
    //~ tomb[0] = t2;
    //~ std::cout << tomb[0].szaml << '/' << tomb[0].nev << std::endl;
}
