#include <iostream>

// ++ operátor:   ++i   i++

// kifejezés, függvény
// - érték    (value)
// - mellékhatás   (side effect)

// érték: szám négyzete
// mellékhatás: kiírt egy sort, vmiféle változás
double negyzet(double x) {
    std::cout << "Négyzetre emelés\n";
    return x*x;
}

// érték: nincs
// mellékhatás: kiír
void kiir(double x) {
    std::cout << x;
}

// érték: duplája
// mellékhatás: nincs
double dupla(double x) {
    return x*2;
}

// érték: véletlenszám, mindig más
// mellékhatás: vmi kell legyen a háttérben
int rand();

int main() {
    
    // operátor: érték? mellékhatás?
    
    // + operátor, érték = összeg, mellékhatás = nincs
    int a = 2, b = 3;
    std::cout << a+b;
    
    // += operátor, érték = összeg, mellékhatás = eredmény a változóba
    a += b;
    
    // = operátor, érték = másolt érték, mellékhatás = változóba bekerül
    a = b;
    
    std::cout << (a = b);       // a = 3,  std::cout << 3
    
    a = b = 10;
    a = (b = 10);       // mellékhatás: b = 10
                        // b = 10 értéke: 10
                        // mellékhatás: a = 10
                        
    ++a;
    a++;                // mellékhatás: a növelése 1-gyel


    // ++a   preinkremens
    a = 5;
    std::cout << ++a;   // mellékhatás: a növelése 1-gyel, a = 6
                        // érték: a már megnövelt(!) érték
                        // kiírva 6 lesz
    std::cout << a;     // kiír: 6
    
    // b++   posztinkremens
    b = 5;
    std::cout << b++;   // mh. b növelése 1-gyel, b = 6
                        // érték: b növelés előtti(!) érték
                        // kiírva 5 lesz
    std::cout << b;     // kiír: 6

    for (int i = 1; i <= 10; ++i)
        std::cout << i;         // 1...10
    for (int i = 1; i <= 10; i++)
        std::cout << i;         // 1...10
    
    // for -> while
    
    int i = 1;
    while (i <= 10) {
        std::cout << i;
        // ciklustörzs végére
        i++;        // ++i
    }
    
    int tomb[100];          // indexek 0..99
    int db = 0;
    while (true) {
        int x;
        std::cin >> x;
        if (x <= 0)
            break;
        tomb[db++] = x;      // 1. mh: x bekerül tömbbe
                             // 2. mh: db növelése
                             // posztinkremens
    }
    
    // felhasználó: 5 6 7 -1
    // [0] -> 5,   [1] -> 6,   [2] -> 7,     [3] -> ide jönne köv.
    // db = 3
    
    int idx;
    for (idx = 0; idx < db; ++idx) {
        std::cout << tomb[idx];     // 0, 1, 2 idx
    }
    
    // sztring összefűzés karaktertömbbökel
    char s1[] = "alma", s2[] = "fa";
    char osszefuz[100];
    
    db = 0;
    for (int i = 0; i < 4; ++i)     // alma 4 betűs  i=0,1,2,3
        osszefuz[db++] = s1[i];
    for (int i = 0; i < 2; ++i)     // fa 2 betűs  i=0,1
        osszefuz[db++] = s2[i];
    
    // [0;4)  alma
    // [0;2)  fa
    // [0;6)  almafa
    //        [0;4)  alma       4-0 -> 4 betű
    //        [4;6)  fa         6-4 -> 2 betű
}




