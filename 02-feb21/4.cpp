#include <iostream>

// tanulság az időtartam osztálynál: a kliens (main) meghatározza
// az interfészt, hogy mit vár ettől az osztálytól:
//   1. óra:perc alapján létre tudjon hozni időtartam objektumot
//   2. össze tudjon adni két időtartamot
//   3. le tudja kérdezni, hány óra hány percről van szó.
// az adattárolás belül mindettől független, az az időtartam osztály
// programozójára van bízva. ezért úgy is dönthet, hogy a hosszt
// egyetlen integerként tárolja; a klienst ez nem érinti (!).
// a fordító el is rejti előle, nem férhet hozzá a privát részhez,
// ezért nem is tud (!) építeni rá, hogy a belső felépítés milyen.
class IdoTartam {
  public:   // interfész
    IdoTartam(int o, int p) {
        hossz = o*60 + p;
    }
    int ora() const {
        return hossz / 60;
    }
    int perc() const {
        return hossz % 60;
    }

    // nem ossz(a, b), hanem a.ossz(b)
    IdoTartam ossz(IdoTartam b) const {
        return IdoTartam(hossz + b.hossz);
    }

  private:
    IdoTartam(int h) {
        hossz = h;
    }
    int hossz;
};

// kliens
int main() {
    IdoTartam i1(1, 40);        // 1ó 40p
    IdoTartam i2(2, 30);        // 2ó 30p
    
    IdoTartam i3 = i1.ossz(i2); // 4ó 10p
    
    std::cout << i3.ora() << ':' << i3.perc();  // 4:10
}
