<h1>Dinamikus linkelés</h1>

<p>Normál, „statikus” linkelés közben a futtatható programba bemásolódnak
a lefordított függvények. Ennek több hátránya van:</p>

<ul>
    <li>Ha egy függvényt átírunk, nem csak a forrásfájlját kell újrafordítani,
    hanem újra is kell linkelni a programot.
    <li>Ha több programban is használjuk ugyanazokat a függvényeket, többször
    lesznek bent a memóriában.
</ul>

<p>Ezeket a problémákat oldja meg a <em>dinamikus linkelés</em> (dynamic 
linking). Ez lényegében annyit jelent, hogy némely forrásfájlokat nem a 
futtatható programfájl előállításakor, hanem csak annak elindításakor linkelünk a 
programhoz. Ezek Unix operációs rendszeren az SO-k (shared object), Windowson 
pedig a DLL-ek (dynamic link library).</p>

<p>Az SO-kból (DLL-ekből) csak egy változat töltődik be a memóriába; ha több
program használja azokat, akkor osztoznak rajta. Így kisebb a memóriafelhasználás.
Könnyebb a cseréjük is: ha az SO-ból új verzió készül, az azt használó programokat
nem kell újralinkelni (amíg persze a benne lévő függvények kompatibilisek maradtak).
Még az is megoldható, hogy egy program a futása közben töltsön be és dobjon el
egy SO-t; így működnek a programok bővítményei (pluginek) és a hardvereszközök
meghajtóprogramjai (driver v. kernel module).</p>

<p>Az SO-k létrehozása és programhoz linkelése majdnem ugyanúgy megy, mint a
szokásos fordítás, csak néhány extra paramétert kell adni a fordítónak, amivel
megadjuk, mit szeretnénk. Ezek közül az egyetlen, aminek elméleti jelentősége
van, a <code>-fPIC</code> paraméter. Ezzel kell jelezni fordítás közben azt, hogy
„PIC = position independent code”-ot szeretnénk létrehozni. Az így lefordított
programba a fordító a globális változók és függvények elérését indirekción
keresztül végzi, ami lehetővé teszi azt, hogy ezek elmozdíthatók legyenek
a memóriában.</p>

<p>Próbáld ki a dinamikus linkelést! Hozd létre az alábbi mini-projektet,
és utána kövesd az utasításokat!</p>

<p>test.c</p>

<pre class="c">
#include <stdio.h>
#include "test.h"

void test(void) {
    printf("Dynamic linking test 1.0\n");
}
</pre>

<p>test.h<p>

<pre class="c">
#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED
void test(void);
#endif
</pre>

<p>main.c</p>

<pre>
#include "test.h"

int main(void) {
    test();
}
</pre>

<p>Először fordítsd le a <code>test.c</code> fájlt! Az ebben lévő függvény
fog a dinamikusan linkelt részbe kerülni. Ennél a fordítási lépésnél kell megadni
a <code>-fpic</code> paramétert:</p>

<pre class="screenshot">
$ gcc <em>-fPIC</em> test.c -c -o test.o
</pre>

<p>Ezután hozd létre az SO (DLL) fájlt! Itt meg lehetne adni több tárgykódot, amik
beépülnek az SO-ba, de most csak egy van:</p>

<pre class="screenshot">
$ gcc <em>-shared</em> test.o -o libtest.so  <span class="bubble">Windowson: -o libtest.dll</span>
</pre>

<p>Végül fordítsd le a főprogramot, és linkeld hozzá dinamikusan a <code>-l</code> paraméterrel a függvénykönyvtárat! A
<code>-L.</code> paraméter azt adja meg, hogy a <code>.so</code> (.dll) fájlt az aktuális mappában kell keresni; mert alapértelmezetten
csak a rendszerszinten definiált mappákban keresné. Az <code>-ltest</code> paraméternél nem kell a név elején lévő <code>lib</code>-et, 
és a név végén lévő <code>.so</code>-t odaírni; azokat automatikusan kezeli. Ezeket megadva a linkelés közben ellenőrzi, hogy 
megtalálhatók-e a függvények. Az <code>rpath</code> a linkernek átadott paraméter (<code>-Wl</code>: a linkernek, <code>rpath
</code>: a <code>-rpath</code> paramétert, az értéke: <code>.</code>); azt a mappát adja meg, ahol az SO fájlt futási időben 
keresni fogja a program. Most ez is az aktuális mappa lesz:</p>

<pre class="screenshot">
$ gcc main.c <em>-L. -ltest -Wl,-rpath,.</em> -o prog
</pre>

<p>Ezután a program elindítható. Az <code>ldd</code> paranccsal ellenőrizni tudod, hogy a létrejött futtatható milyen 
könyvtárakhoz kapcsolódik, ezek között látszódnia kell a <code>libtest.so</code>-nak is, és nem lehet mellette „not found” 
jelzés:</p>

<pre class="screenshot">
$ ldd prog
</pre>

<p>Próbáld ki, hogy tényleg dinamikus-e a linkelés! Változtasd meg a <code>test()</code>
függvényt, írjon ki ez új verziószámot! Fordítsd utána a <code>test.c</code>-t újra,
és állítsd elő a <code>libtest.so</code> új verzióját is. A <code>./prog</code>
programot futtatva ezután rögtön az új verziószám kell látszódjon.</p>

