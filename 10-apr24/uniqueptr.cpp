// lényege: magától felszabadítja a kezelt objektumot

template <typename T>
class UniquePtr {
    private:
        T *data;
    public:
        UniquePtr(T *data) {        // new T() 
            this->data = data;
        }
        ~UniquePtr() {
            delete data;
        }
};
