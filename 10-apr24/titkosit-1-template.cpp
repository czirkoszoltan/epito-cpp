#include <iostream>

class Rot13 {
  public:
    char operator() (char c) {
        if (c >= 'a' && c <= 'z') {
            return (c - 'a' + 13) % 26 + 'a';
        } else {
            return c;
        }
    }
};


class Tukroz {
  public:
    char operator() (char c) {
        if (c >= 'a' && c <= 'z') {
            return 'z' - (c - 'a');
        } else {
            return c;
        }
    }
};

class Lepteto {
  private:
    int eltolas = 0;
  public:
    char operator() (char c) {      // functor
        if (c >= 'a' && c <= 'z') {
            eltolas += 1;
            return (c - 'a' + eltolas) % 26 + 'a';
        } else {
            return c;
        }
    }
};


template <typename FUNC>
void titkosit_szoveget(std::string & s, FUNC func) {
    for (char & c : s)
        c = func(c);
}


int main() {
    std::string s = "hello vilag!";
    std::cout << s << std::endl;
    Rot13 t;
    titkosit_szoveget(s, t);
    std::cout << s << std::endl;
}
