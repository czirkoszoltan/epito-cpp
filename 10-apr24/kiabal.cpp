#include <iostream>
#include <memory>

class Kiabal {
    public:
        Kiabal() {
            std::cout << "letrejott\n";
        }
        ~Kiabal() {
            std::cout << "megszunt\n";
        }
};

class Alap {
    Kiabal k1, k2;
  public:
    virtual ~Alap() = default;  // megcsinálja a fordító: ~Alap() {  k2 ~Kiabal(), k1 ~Kiabal()  }
};

class Leszarmazott : public Alap {
    Kiabal k3;
    
    // megcsinálja a fordító:  ~Leszarmazott() { k3 ~Kiabal(), ~Alap()  }
};



int main() {
    std::unique_ptr<Alap> p;
    p = std::make_unique<Leszarmazott>();
}
