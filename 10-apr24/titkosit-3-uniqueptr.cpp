#include <iostream>
#include <memory>


class KarakterTitkosito {
  public:
    virtual char operator() (char c) = 0;
    virtual ~KarakterTitkosito() = default;
};


void titkosit_szoveget(std::string & s, KarakterTitkosito & func) {
    for (char & c : s)
        c = func(c);        // futási időben dől el!  virtual fv
}












class Rot13 : public KarakterTitkosito {
  public:
    char operator() (char c) override {
        if (c >= 'a' && c <= 'z') {
            return (c - 'a' + 13) % 26 + 'a';
        } else {
            return c;
        }
    }
};


class Tukroz : public KarakterTitkosito {
  public:
    char operator() (char c) override {
        if (c >= 'a' && c <= 'z') {
            return 'z' - (c - 'a');
        } else {
            return c;
        }
    }
};

class Lepteto : public KarakterTitkosito {
  private:
    int eltolas = 0;
  public:
    char operator() (char c) override {      // functor
        if (c >= 'a' && c <= 'z') {
            eltolas += 1;
            return (c - 'a' + eltolas) % 26 + 'a';
        } else {
            return c;
        }
    }
};


class SzamokatIsTukroz : public KarakterTitkosito {
  public:
    char operator() (char c) override {
        if (c >= 'a' && c <= 'z') {
            return 'z' - (c - 'a');
        } else if (c >= '0' && c <= '9') {
            return '9' - (c - '0');
        } else {
            return c;
        }
    }
};


std::unique_ptr<KarakterTitkosito> valaszt() {
    std::cout << "rot13(r) vagy tukroz(t)? ";
    char c;
    std::cin >> c;
    if (c == 'r') {
        return std::make_unique<Lepteto>();
    } else {
        return std::make_unique<SzamokatIsTukroz>();        // new SzamokatIsTukroz()
    }
}


int main() {
    std::string s = "hello 324789 vilag!";
    std::cout << s << std::endl;
    
    std::unique_ptr<KarakterTitkosito> t = valaszt();  // new Rot13, new Tukroz, ...
    titkosit_szoveget(s, *t);
    
    std::cout << s << std::endl;
}   // t destruktor -> delete t
