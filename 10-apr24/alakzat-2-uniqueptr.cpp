#include <iostream>
#include <vector>
#include <memory>


// alaposztály, ősosztály, base class
class Alakzat {
  protected:
    int x, y;
    int szin = 123;
  public:
    void mozgat(int dx, int dy) {
        x += dx;
        y += dy;
    }
    
    virtual double kerulet() const = 0;
    virtual double terulet() const = 0;
    virtual ~Alakzat() {}   // van virtuális fv -> kell virtuális destruktor
};

//~ class Sokszog : public Alakzat {
  //~ private:
    //~ std::vector<Pont> csucsok;
  //~ public:
    //~ virtual double kerulet() const { return 0; }
    //~ virtual double terulet() const { return 0; }
//~ };

//~ Alakzat * p = new Sokszog();
//~ // ...
//~ delete p;   // p -> Alakzat* -> ~Sokszog() destruktor hívása!!!


// leszármazott osztály, derived class, subclass
class Teglalap : public Alakzat {
  private:
    int sz, m;
  public:
    Teglalap(int x, int y, int sz, int m) {
        this->x = x;
        this->y = y;
        this->sz = sz;
        this->m = m;
    }
    double kerulet() const override {
        return 2 * (sz + m);
    }
    double terulet() const override {
        return sz * m;
    }
};


class Kor : public Alakzat {
  private:
    int r;
  public:
    Kor(int x, int y, int r) {
        this->x = x;
        this->y = y;
        this->r = r;
    }
    double kerulet() const override {
        return 2 * 3.14 * r;
    }
    double terulet() const override {
        return 3.14 * r * r;
    }
};


// Alakzat & a
// polimorf viselkedés
void alakzat_kiir(Alakzat & a) {
    std::cout << "kerület = " << a.kerulet() << ", "
                 "terület = " << a.terulet() << std::endl;
}


std::unique_ptr<Alakzat> alakzat_letrehoz() {
    std::cout << "(k)ör vagy (t)églalap vagy x=kilép? ";
    char c;
    std::cin >> c;
    if (c == 'k')
        return std::make_unique<Kor>(200, 300, 10);
    if (c == 't')
        return std::make_unique<Teglalap>(200, 300, 10, 30);
    return nullptr;
}


template <typename T>
class UniquePtr {
    private:
        T *data;
    public:
        UniquePtr(T *data) {        // new T() 
            this->data = data;
        }
        ~UniquePtr() {
            delete data;
        }
};


int main() {
    // heterogén kollekció
    // polymorphic container
    std::vector<std::unique_ptr<Alakzat>> v;
    while (true) {
        std::unique_ptr<Alakzat> a = alakzat_letrehoz();
        if (a == nullptr)
            break;
        v.push_back(std::move(a));
    }
    
    for (auto & p : v)
        alakzat_kiir(*p);
}



