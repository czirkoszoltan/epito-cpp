struct RekordTeljes {
    int adat1;
    float adat2;
    double adat3;
    int adat4;
};

std::vector<RekordTeljes> osszes_adat = {...};



struct RekordReszleges {
    float adat2;
    int adat4;
    
    double uj_adat;
    
    // hogy létre tudjak hozni magában is
    RekordReszleges() = default;
    
    // Teljes->Részleges konverzióhoz
    RekordReszleges(RekordTeljes const & rt) {
        this->adat2 = rt.adat2;
        this->adat4 = rt.adat4;
    }
};

std::vector<RekordReszleges> reszleges;
reszleges.reserve(osszes_adat.size());

for (RekordTeljes & rt : osszes_adat)
    reszleges.push_back(rt);        // Teljes->Részleges konverzió a konstruktorral!
