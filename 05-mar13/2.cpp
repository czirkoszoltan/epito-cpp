#include <iostream>
#include <cstring>
#include <string>

char * osszefuz(char *p1, char *p2) {
    size_t h1 = strlen(p1);
    size_t h2 = strlen(p2);
    size_t hossz = h1+h2;

    // dinamikus sztring, két okból:
    // - futási időben tudjuk csak a méretet meghatározni
    // - maradjon meg, amikor visszatértünk a függvényből
    char *ossze = new char[hossz + 1];    // lezáró nulla miatt +1

    // "alma0xx"
    strcpy(ossze, p1);
    // "alma0xx"
    //      ^ innentől kezdve írjuk tovább
    // "almaFA0"
    strcpy(ossze + h1, p2);
    
    return ossze;
}

int main() {
    char str1[5] = "alma";
                 // ^p1
    char str2[3] = "FA";
                 // ^p2
    char *p = osszefuz(str1, str2);        // almaFA
    std::cout << p;
    delete[] p;
    return 0;
}
