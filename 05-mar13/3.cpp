#include <iostream>
#include <cstring>

// sztring osztály. félkész!
class String {
    private:
        size_t hossz;
        char *szoveg;
    public:
        // konstruktor. az alapértelmezett paraméterérték
        // miatt default konstruktor is.
        String(char const *kezdeti = "") {
            // mennyi a sztring hossza? annyi +1 méretű tömb kell
            // (a lezáró \0 miatt, amit a strcpy tesz oda)
            hossz = strlen(kezdeti);
            szoveg = new char[hossz + 1];
            strcpy(szoveg, kezdeti);
        }
        
        ~String() {     
            // destruktor, automatikusan fut, amikor megszűnik az objektum.
            // itt a konstruktorban lévő dinamikusan foglalt new char[] párja!
            delete[] szoveg;
        }
        
        char & operator[] (size_t idx) {
            return szoveg[idx];
        }
        char const & operator[] (size_t idx) const {
            // overload az indexelő operátor számára. az objektum
            // konstansságára is lehet overloadolni!
            // fontos, hogy a visszatérési értékbe is const került:
            // ha a sztring konstans (jobb szélen lévő const),
            // akkor a visszaadott karakterre is úgy mutassunk rá,
            // hogy ne lehessen megváltoztatni (bal szélen const)
            return szoveg[idx];
        }
        
        size_t get_hossz() const {
            return hossz;
        }
        
        void operator+= (char c) {
            // karakter hozzáfűzése. eggyel nagyobb tömb kell,
            // régi hossz, +1 az új karakternek, +1 a lezáró nullának
            char *uj = new char[hossz + 1 + 1];
            strcpy(uj, szoveg);
            uj[hossz] = c;
            uj[hossz + 1] = '\0';
            // régi tömb nem kell; a this->szoveg pedig átállítható
            // az új tömbre.
            delete[] szoveg;
            szoveg = uj;
            hossz += 1;
        }
};

std::ostream & operator<< (std::ostream & os, String const & s) {
    for (size_t i = 0; i < s.get_hossz(); ++i)
        os << i << " " << s[i] << std::endl;
    return os;
}

int main() {
    String s1 = "alma";     // String(char const *) constructor, new char []
    
    s1[0] = 'A';            // s1.operator[](0) = 'A';
    
    std::cout << s1;        // overload: ostream << String
    
    s1 += '!';              // s1.operator+= ('!')
    
    std::cout << s1;
}
