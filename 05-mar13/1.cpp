int x = 3;
int y = 4;

int *p;      // pointer; a * a típusnév része
p = &x;      // &: címképző operátor, nem keverendő a referenciákkal!
             // p mostantól x-re mutat
*p = 5;      // dereferálás, a mutatott int 5 lesz (x=5)
p = &y;      // p mostanról y-ra mutat
*p = 6;      // most az y lesz 6

p = nullptr; // null pointer, semmire sem mutat

    // p = 0;       // régi stílus, nem használjuk már
    // p = NULL;    // régi stílus, nem használjuk már

// ---------------------------------------------

int array[100];

int *p = array; // p a kezdőelemre mutat, mint p = &array[0]
int *p2 = p+7;  // ugrás 17 elemmel előre, mint &array[7]
int *p3 = p2-5; // 5 elemmel hátra, mint &array[2]

*(p+10) = 18;   // array[10] = 18; figyelem: precedencia!
p[10] = 18;     // ugyanaz, mint fent; [] címet számít és dereferál

// ---------------------------------------------

void string_copy(char *dest, char const *src) {
    size_t i;
    for (i = 0; src[i] != '\0'; ++i)
        dest[i] = src[i];
    dest[i] = '\0';
}

void test() {
    char source[] = "hello";    // h, e, l, l, o, \0
    char destination[100];
    string_copy(destination, source);   // tömb->ptr konverzió!
    std::cout << destination;           // tömb->ptr konverzió!
}

// ---------------------------------------------

int *array = new int[100];      // tömb foglalása
delete[] array;                 // tömb felszabadítása. fontos a []!

