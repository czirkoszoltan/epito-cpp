class Tevekenyseg {
    virtual void elvegzes() = 0;
};

class BalraFordul : public Tevekenyseg {
    virtual void elvegzes() {
        szog += 90;
    }
};

class EloreMegy : public Tevekenyseg {
    virtual void elvegzes() {
        x += 10;
    }
};


class Szenzor {
    virtual bool jelez() const = 0;
};

class Hangszenzor : public Szenzor {
    virtual bool jelez() const {
        ...;
    }
};




void f(std::vector<Tevekenyseg*> tev_k, Szenzor & sz) {
    if (sz.jelez())
        for (auto * t : tev_k)
            t->tevekenyseg();
}

