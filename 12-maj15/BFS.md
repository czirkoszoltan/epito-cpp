# BFS

Gráf:

![](graf.svg)

Éllistás tárolás:

![](graf_eltomb.svg)

C++ reprezentáció:

```c++
class El {
    int hova;
    std::string nev;
    double suly;
};

class Csucs {
    std::vector<El> elek;   // kimeno
    // std::vector<int> bejovo;
	std::string nev;
};

class Graf {
    std::string nev;
    std::vector<Csucs> csucsok;    
};

// gráf építése
Graf g1;
g1.csucsok.push_back(Csucs("A"));   // 0
g1.csucsok.push_back(Csucs("B"));   // 1
g1.csucsok[0].elek.push_back(El(1, 12.46));    // A->B
g1.csucsok[0].elek.push_back(El(1, 1.56));    // A->B

// él törlése: gyors lehet, az éllista végével felülírni
// a törlendő elemet, aztán a végét eldobni. akkor, ha nem számít a sorrend!
g1.csucsok[0].elek[2] = g1.csucsok[0].elek.back();
g1.csucsok[0].elek.pop_back();

// for (Csucs & cs : g1.csucsok)
//    for (El & e : cs.elek)

// minden él bejárása:

// minden csúcsra...
for (size_t cs = 0; cs < g1.csucsok.size(); ++cs) {
    Csucs & csucs = g1.csucsok[cs];
    // onnan kiinduló minden élre...
    for (size_t ei = 0; ei < csucs.elek.size(); ++ei) {
        El & el = csucs.elek[ei];
        std::cout << "honnan = " << cs
                  << "hova = " << el.hova;
    }
}
```

## BFS 1.0

BFS = breadth first search:

![](bfs1.svg)

Algo:

```
s-t bejártnak jelölni
Q = [s]
AMÍG Q ≠ ∅:
    v = Q elejéről kivenni
    MINDEN v → w élre:
        HA w nincs bejárva:
            w-t bejártnak jelölni
            Q végére w-t betenni
            <tevékenység v → w élre>
    <tevékenység v csúcsra>
```

C++ algo:

```c++
void bfs(Graf &g, int start) {
	std::vector<bool> bejart(g.csucs.size(), false);   // csúcsok_száma/8 bájt
    bejart[start] = true;
    std::queue<int> sor = { start };
    while (!sor.empty()) {
        int v = sor.front();  sor.pop();  // O(~1)
        for (El & el : g.csucs[v]) {
            int w = el.hova;
            if (bejart[w] == false) {     // O(1)
                bejart[w] = true;         // O(1)
                sor.push(w);              // O(~1)
                // ...
            }
        }
        // ...
    }
}
```

## BFS 2.0

![](bfs_szintek.svg)

A sor:

```
←  (0) ... (1 4 2) ... (3 5 6) ... (8 7)  ←

s-t bejártnak jelölni
front = [s]
AMÍG Q ≠ ∅:
    uj_front = []
    MINDEN v élre: front
        MINDEN v → w élre:
            HA w nincs bejárva:
                w-t bejártnak jelölni
                uj_front += [w]
                <tevékenység v → w élre>
        <tevékenység v csúcsra>
    front = uj_front
```

Front = amelyik csúcsoknál épp tartunk; amik az előző szint miatt bekerültek a sorba.

Módosított C++:

```c++
void bfs(Graf &g, int start) {
	std::vector<bool> bejart(g.csucs.size(), false);   // csúcsok_száma/8 bájt
    bejart[start] = true;

    std::vector<int> front = { start };
    while (!front.empty()) {
        std::vector<int> ujfront;
        for (int v : front) {
            for (El & el : g.csucs[v]) {
                int w = el.hova;
                if (bejart[w] == false) {     // O(1)
                    bejart[w] = true;         // O(1)
                    ujfront.push_back(w);     // O(~1)
                    // ...
                }
            }
        }
        front = std::move(ujfront);
        // ...
    }
}
```

Magyarázatok: https://infopy.eet.bme.hu/grafok/
