#include <iostream>

// Problémafelvetés:
// - Köröket és téglalapokat szeretnék tárolni.
// - Nagyon sok a közös kód: koordináták, színek, mozgatás...
// - Hogy oldom meg, hogy egy függvény kaphasson kört
//   és téglalapot is?

class Teglalap {
  private:
    int x, y;
    int sz, m;
    int szin = 123;
  public:
    Teglalap(int x, int y, int sz, int m) {
        this->x = x;
        this->y = y;
        this->sz = sz;
        this->m = m;
    }
    double kerulet() const {
        return 2 * (sz + m);
    }
};

class Kor {
    int x, y;
    int r;
    int szin = 235;
  public:
    Kor(int x, int y, int r) {
        this->x = x;
        this->y = y;
        this->r = r;
    }
    double kerulet() const {
        return 2 * 3.14 * r;
    }
};

void kerulet_kiir(Alakzat??? a) {
    std::cout << a.kerulet() << std::endl;
}

int main() {
    Kor k1(200, 300, 10);
    Teglalap t1(200, 300, 10, 30);

    std::cout << k1.kerulet() << std::endl;
    std::cout << t1.kerulet() << std::endl;
}
