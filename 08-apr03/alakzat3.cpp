// Hibás leszármazások.
//
// Vigyázat, objektumorientált programozásban
// a leszármazás nem egyezik meg a matematikai
// és a rendszertani fogalmakkal!
//
// OOP: amit az ős tud, azt a leszármazottnak is
// tudnia kell. Ahol ős típusú paramétert vár
// a függvény, ott jól kell működjön a program,
// ha leszármazottat kap.

// Biológia: a stucc madár.
// OOP: a strucc nem madár!
// - A madarak tudnak repülni.
// - A strucc egy madár.
// - Következtetés: a strucc tud repülni.
class Madar {
    void repul() {
        std::cout << "repülök";
    }
};

class Strucc : public Madar {
};

Strucc s1;
s1.repul();


// Matematika: a négyzet egy fajta téglalap.
// OOP: a négyzet NEM téglalap.
// - A téglalapok megnyúlhatnak széltében,
//   miközben a magasságuk nem változik.
// - A négyzet egy fajta téglalap
// - Következmény: a négyzet nyúlhat széltében,
//   magassága nem változik.
// A téglalap definíciója NEM AZ, amit matematikából
// tanultam, hanem az, amit a kódba írtam!!!
class Teglalap {
    int sz, m;
    Teglalap(int sz, int m) {
        this->sz = sz;
        this->m = m;
    }
    int kerulet() {
        return 2 * (sz + m);
    }
    void szelteben_megnyujt(int d) {
        sz *= d;
    }
};

class Negyzet : public Teglalap {
    Negyzet(int a) {
        this->sz = a;
        this->m = a;
    }
};

// Matematika: az ellipszis egy fajta kör.
// OOP: az ellipszis megörökli a get_sugar()
// függvényt, vagyis le lehet kérdezni az ellipszis
// sugarát... Pedig nincs is neki olyanja.
class Kor {
    int r;
    int get_sugar() { return r; }
};

class Ellipszis : public Kor {
    int r2;
};

Ellipszis e1;
e1.get_sugar();
