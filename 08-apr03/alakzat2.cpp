#include <iostream>

// Megoldás: örökléssel.
// Alakzat *ősosztályból* leszármazik Téglalap és kör.
// Az alakzat a terület és a kerület kiszámításához
// tisztán virtuális függvény tartalmaz; a *leszármazottak*
// megmondják, hogy konkrét esetben ezt a műveletet
// hogyan kell elvégezni.

// alaposztály, ősosztály, base class
// absztrakt alaposztály, abstract base class    abc
class Alakzat {
  protected:
    int x, y;
    int szin = 123;
  public:
    void mozgat(int dx, int dy) {
        x += dx;
        y += dy;
    }
    
    // polimorf viselkedés
    // tisztán virtuális függvény, pure virtual function
    // ígéret a leszármazottak nevében
    // virtual -> rejtett adattag: kerulet fv-re mutató pointer
    virtual double kerulet() const = 0;
    virtual double terulet() const = 0;
};


// leszármazott osztály, derived class, subclass
// a Téglalap egy fajta Alakzat;
// ahova alakzat kell, oda téglalap is jó
class Teglalap : public Alakzat {
  private:
    int sz, m;
  public:
    Teglalap(int x, int y, int sz, int m) {
        this->x = x;
        this->y = y;
        this->sz = sz;
        this->m = m;
    }
    double kerulet() const override {
        return 2 * (sz + m);
    }
    double terulet() const override {
        return sz * m;
    }
};


class Kor : public Alakzat {
  private:
    int r;
  public:
    Kor(int x, int y, int r) {
        this->x = x;
        this->y = y;
        this->r = r;
    }
    double kerulet() const override {
        return 2 * 3.14 * r;
    }
    double terulet() const override {
        return 3.14 * r * r;
    }
};


// Alakzat & a
// polimorf viselkedés
//   statikus típus: ami a forráskódban látszik -> Alakzat
//   dinamikus típus: ami a ténylegesen ott van -> Kor, Teglalap
// Ennek a függvénynek mindegy, milyen konkrét alakzatot kapott.
// Legyen az téglalap vagy kör, jól működik vele
void alakzat_kiir(Alakzat & a) {
    std::cout << "kerület = " << a.kerulet() << std::endl;
    std::cout << "terület = " << a.terulet() << std::endl;
}

int main() {
    
    Kor k1(200, 300, 10);
    Teglalap t1(200, 300, 10, 30);
    
    // statikus típus: ami a forráskódban látszik -> Alakzat
    // dinamikus típus: ami a ténylegesen ott van -> Kor, Teglalap
    // Alakzat & a1 = k1;      // k1 -> Kor& -> Alakzat&
    // Alakzat & a2 = t1;      // t1 -> Teglalap& -> Alakzat&

    // Ugyanilyen Teglalap& -> Alakzat& konverzió
    // és Kor& -> Alakzat& konverzió van a paraméterátadásnál:
    alakzat_kiir(k1);    // 62.8
    alakzat_kiir(t1);    // 80
}
