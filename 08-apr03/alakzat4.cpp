// OOP tábla

class Sakktabla {
    Babu palya[8][8];
};

// Játékos ősosztály: megmutatjuk neki a pályát,
// és ő lép valahogy.
class Jatekos {
    virtual void lep(Sakktabla & s) = 0;
};

// Ember játékos hogy valósítja ezt meg?
// Kirajzolja a táblát, és a felhasználó megad
// koordinátákat.
class EmberJatekos : public Jatekos {
    virtual void lep(Sakktabla & s) {
        int x, y;
        palya_kirajzol(s);
        std::cin >> x >> y;
    }
};

// Gépi játékos? Elemzi az állást, pontozza,
// állásoka vizsgál stb.
class GepJatekos : public Jatekos {
    virtual void lep(Sakktabla & s) {
        // .... 
    }
};

// Replay játékos: fájlból olvassa a lépéseket
class Replay : public Jatekos {
    virtual void lep(Sakktabla & s) {
        // .... 
    }
};

// Főprogram: a kódban teljesen lényegtelen,
// hogy emberjátékos, gépjátékos játszik egymás
// ellen, vagy épp fájlba mentett játszmát
// rajzolunk ki lépésenként.
Sakktabla tabla;
Jatekos & j1 = ...;
Jatekos & j2 = ...;
while (!van_nyertes) {
    j1.lep(tabla);
    j2.lep(tabla);
}
