# Titkosítás

Adott egy titkosító algoritmus. Ez egy sztring összes
betűjét titkosítja (csak a betűket, minden mást kihagy):

```c++
void kodol(std::string & s, Titkosito & t) {
    for (char & c : s)
        if (c >= 'a' && c <= 'z')
            c = t.kodol(c);
}
```

A dekódolás ugyanilyen:

```c++
void dekodol(std::string & s, Titkosito & t) {
    for (char & c : s)
        if (c >= 'a' && c <= 'z')
            c = t.dekodol(c);
}
```

A titkosítás (kódolás) és a feloldás (dekódolás) lépésben
a karakterek kezelését a `Titkosito` objektum adja meg.

Lehetséges titkosítások:

- Minden betű helyett az ábécé végéről: a=z, b=y, c=x és így tovább.
- Minden betű helyett az ábécében következő: a=b, b=c és így tovább.
- Titkosítás egy szóval, mint kulcs: ha jelszó cica, akkor
  az első betűt 2-vel toljuk el (a→c), a másodikat 8-cal (a→i),
  a harmadikat megint 2-vel, a negyedik marad változatlan; a többi
  betűt rendre ugyanígy, mindig visszatérve a jelszó elejére
  (mintha cicacicacica... lenne a jelszó).

Készíts titkosító alaposztályt kódol és dekódol függvénnyel, amelyek
egyetlen egy karaktert kezelnek.

Készíts leszármazott osztályokat, amelyek a fenti módokon megvalósítják
a kódolást és dekódolást! Az első kettő leszármazottnak nem lesz adattagja,
a harmadiknak viszont adattag is kell, amelyik a jelszót tárolja, és
még egy, amelyik számon tartja, hogy hányadik karakternél tart.
