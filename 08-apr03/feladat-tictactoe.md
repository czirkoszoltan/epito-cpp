# Tic-tac-toe

Tic-tac-toe (3×3-as amőba) játékot kell írni.

Adott a következő játékállás és pozíció osztály:

```c++
struct Jatekallas {
    char palya[3][3];   // ' ' vagy 'x' vagy 'o'
    int kovetkezo;
};

struct Pozicio {
    int x, y;
};
```

És adott a következő játékos ősosztály:

```c++
class Jatekos {
  public:
    virtual Pozicio lep(Jatekallas & j) = 0;
};
```

A játékot vezérlő programrész két játékost kérdezget
felváltva, hova szeretnének lépni a pályán. Az
emberjátékos megvalósítás így működik:

- Kirajzolja a pályát
- Kér koordinátákat a felhasználótól.

A (kezdetleges) gépjátékos így működik:

- Találomra választ egy üres helyet, és oda lép.

Írd meg azt a főprogramot, amelyik paraméterként
kap két játékost, létrehoz egy üres pályát, utána
pedig azon "engedi" felváltva lépni a játékosokat:

```c++
void jatek(Jatekos &j1, Jatekos &j2) {
    Jatekallas allas;
    while (...) {
        Pozicio lepes;
        lepes = j1.lep();
        ...
        lepes = j2.lep();
        ...
    }
}
```

Valósítsd meg a két játékos osztályt is!

Írj okosabb gépjátékos osztályt, amelyik megvizsgálja
a pályát, és a véletlenszerű lépés helyett:

- Ha olyan helyre tud lépni, amivel nyerni tud, oda lép.
- Ha olyan helyre tud lépni, amivel egy 2-esét lezárja az ellenfélnek, azt lépi.
- Amúgy véletlenszerűen lép.