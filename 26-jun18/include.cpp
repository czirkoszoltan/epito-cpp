#include <iostream>

// egyesével
#include <gtk/gtkbutton.h>
#include <gtk/gtktextentry.h>
#include <gtk/gtkcheckbox.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkscrollbar.h>
#include <gtk/gtkwindow.h>

// mindent - ugyanazt csinálja, mint a fenti
// - kényelmesebb
// - lassabb fordítás
#include <gtk/gtk.h>

int main() {
    GtkButton *mybutton = ...;
    GtkWindow *mywindow = ...;
    GtkTextEntry *myentry = ...;
}
