// szabványos
#include <fstream>

// saját
#include "model/reservation.h"
#include "process/missing.h"        // #include "model/reservation.h"
#include "files/textreader.h"
#include "files/csvreader.h"


int main() {
    using namespace Model;
    using namespace std;
    
    ifstream is("input.txt");
    // scope operator "négyespont operátor"
    vector<Reservation> first = Files::read_reservations_text(is);
    vector<Reservation> second = Files::read_reservations_text(is);
    is.close();
    
    vector<ReservationId> missing = Process::find_missing_reservation(first, second);
    
    for (ReservationId id : missing)
        std::cout << id << ", ";
}

