#pragma once

namespace Model {
    // weak alias
    using ReservationId = int;
    using TimeStamp = int;

    // class
    struct Reservation {
        ReservationId id;
        TimeStamp time;  
    };
}

