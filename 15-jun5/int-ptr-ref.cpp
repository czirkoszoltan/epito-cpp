

int fuggveny_1(int szam) {
    return szam*szam;
}

int fuggveny_2(int *szam) {
    return (*szam) * (*szam);
}

int fuggveny_3(int &szam) {
    return szam*szam;
}

// ha tomb[] van a függvény fejlécében, a fordító mindig
// pointert képzel oda helyette! vagyis ez olyan, mintha
// int fuggveny_4(int *tomb) lenne a fejlécben.
int fuggveny_4(int tomb[]) {
    tomb[12] = 4578;        // *(tomb+12) = 4578
}

// mit jelent ez? precedenciákon múlik. mivel [] > *,
// ezért ez tömb, ami pointereket tartalmaz.
// de ha tömb van függvény fejlécében, azt pointerrel
// helyettesíti, vagyis int fuggveny_5(int **tomb)
int fuggveny_5(int *tomb[]) {       // int fuggveny_5(int **tomb)
}

// ez referenciák tömbje lenne, de az nem létezik.
//~ int fuggveny_6(int &tomb[]) {
//~ }


int main1() {
    int i = 12;
    int &r = i;
    
    fuggveny_1(i);
    fuggveny_3(i);
    
    int* p = &i;
    fuggveny_2( &i );
}


int main1() {
    
    int* x[123]  ;      // pointerek tömbje
    
    *x[15]  = 45678;
    *(x[15])  = 45678;
    //~ (*x)[15]  = 45678;
    
    
    1 + 2 * 3
    1 + (2 * 3)     választott
    //~ (1 + 2) * 3
    
    // referenciák tömbje és referenciára mutató pointer
    // nem létezhet, mert a referencia nem objektum.
    // int & tomb[100];
    //~ int& * p;
}
