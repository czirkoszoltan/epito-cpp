#include <iostream>
#include <stdexcept>
#include <vector>

int osztas(int a, int b) {
    if (b == 0)       // hiba érzékelésének a helye
        throw std::invalid_argument("nullával nem lehet osztani");
    
    return a / b;
}

void hanyados_kiir(int a, int b) {
    std::cout << osztas(a, b) << std::endl;
}


int main() {
    int x, y;
    std::cin >> x >> y;
    
    try {
        hanyados_kiir(x, y);
        std::cout << "minden ok volt" << std::endl;
    } catch(std::exception & e) {      // hiba kezelésének a helye
                                       // std::invalid_argument leszármazottja std::exception-nek
        std::cout << "hiba történt, " << e.what() << std::endl;
    }
    
    std::vector<int> v(100);
    try {
        v.at(1234) = 98;               // v[1234], de ellenőrzi a túlindexelést
    } catch (std::exception & e) {
        std::cout << e.what() << std::endl;
    }
}


