// mátrix beolvasása,
// - kézi erőforráskezeléssel (memória, fájlbezárás)
// - kézi hibakezeléssel
double **read_matrix_1(char const *filename) {
    FILE *fp = fopen(filename, "rt");
    if (fp == NULL)
        return NULL;
    int w, h;
    if (fscanf(fp, "%d %d", &w, &h) != 2) {
        fclose(fp);
        return NULL;
    }
    double ** ret = (double **) malloc(sizeof(double*) * h);
    if (ret == NULL) {
        fclose(fp);
        return NULL;
    }
    for (int y = 0; y < h; ++y) {
        ret[y] = (double *) malloc(sizeof(double) * w);
        if (ret[y] == NULL) {
            for (int yy = 0; yy < y; ++yy)
                free(ret[yy]);
            free(ret);
            fclose(fp);
            return NULL;
        }
    }
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            if (fscanf(fp, "%lf", &ret[y][x]) != 1) {
                for (int yy = 0; yy < h; ++yy)
                    free(ret[yy]);
                free(ret);
                fclose(fp);
                return NULL;
            }
        }
    }
    fclose(fp);
    return ret;
}

// mátrix beolvasása,
// - automatikus erőforráskezeléssel (file destruktora bezárja a fájlt, matrix konstruktor/destruktor kezeli a memóriát)
// - de még kézi hibakezeléssel (sikeres-e a fájlok megnyitása, számok beolvasása)
// ami fontos itt: eltűnt az erőforráskezelés, és ez nagyon leegyszerűsíti
// a programot! nem kell figyelni rá, mi a felszabadítandó memóriaterület,
// bezárandó fájl; ezek eltűntek.
Matrix read_matrix_2(char const *filename) {
    std::ifstream file;
    file.open(filename);
    if (!file.is_open())
        throw std::runtime_error("nem nyitható meg");
    int w, h;
    if (!(file >> w >> h))
        throw std::runtime_error("hibás méretek");
    Matrix ret(w, h);
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
            if (!(file >> ret(x, y)))
                throw std::runtime_error("hibás számok");
    return ret;
}

// mátrix beolvasása,
// - automatikus erőforráskezeléssel
// - automatikus hibakezeléssel
// mivel minden fájlművelet kivételt dob magától, ha hiba van,
// és minden erőforrás felszabadul magától, ha kivétel dobódott
// (a 'file' és a 'ret' objektumok destruktorai miatt),
// így *teljesen* eltűnt látszólag a hibakezelés; a feladatra
// tudunk koncentrálni.
Matrix read_matrix_3(char const *filename) {
    std::ifstream file;
    file.exceptions(std::ifstream::badbit | std::ifstream::eofbit | std::ifstream::failbit);
    file.open(filename);
    int w, h;
    file >> w >> h;
    Matrix ret(w, h);
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
            file >> ret(x, y);
    return ret;
}
