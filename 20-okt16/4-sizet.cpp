// kettes komplemens
// bináris számábrázolás

// uint_8
// 00000000 - 11111111    0...255

// int_8   előjeles!
// 0       |0000000
// ^ előjel ^szám
//                      -128 ... +127

int main() {
    
    signed int   i = 3;     // int i
    unsigned int u = 4;     // size_t ~ unsigned int    v.size() -> size_t
    
    if (i < u) {
    }
    //~ for (size_t i = 0; i < v.size(); ++i) {
    //~ }
    
    for (size_t i = 0; i < (int)v.size(); ++i) {
    }
    
    for (size_t i = v.size(); i > 0; --i) {
    }
    for (int i = v.size(); i > 0; --i) {
    }
}
