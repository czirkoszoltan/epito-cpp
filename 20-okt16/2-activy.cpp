
			activity(act_ellistas const& data1) // Ez szerintem azt jelenti, hogy az actdata tömb adataival fogja feltölteni amikor létrejön a példány
			{
                this->uid = data1.uid;
                this->Dur_Data = data1.Dur;	// MIÉRT NEM MÚKÖDIK EZ A THIS ->  MEG AZ ÖSSZES TÖ
                this->Num_Of_IN_LINKS = data1.NotHandledPredecessors;
                this->Not_Handled_IN_LINKS = data1.NotHandledPredecessors;
                this->Num_Of_OUT_LINKS = data1.NotHandledSuccessors;
                this->Not_Handled_OUT_LINKS = data1.NotHandledSuccessors;

                this->Num_Of_OUT_LINKS= data1.NotHandledSuccessors;
                this->links_OUT.resize(data1.NotHandledSuccessors);
                for (int j = 0; j < data1.NotHandledPredecessors; j++)
                {
                    this->links_IN[j].FromNode= data1.Predecessors[j].FromNode;
                    this->links_IN[j].LAG = Transform_ONE_LINK_To_SSE_Equivalent(i, j, 0);
                }
                for (int j = 0; j < data1.NotHandledSuccessors; j++)      
                {
                    this->links_OUT[j].ToNode = data1.Successors[j].ToNode;
                    this->links_OUT[j].LAG = Transform_ONE_LINK_To_SSE_Equivalent(i, j, 1);
                }
			};


// globális
// extern vector<act_ellistas> ellistas;



void ELLISTAS_DIJKSTRA_minSSE::COPY_DATA_INTO_act(vector<act_ellistas> const & ellistas)
{
    act.resize(ellistas.size());
    for (int i = 0; i < ellistas.size(); i++) {
        act[i] = activity(ellistas[i]);
    }
}

int main() {
    vector<act_ellistas> ellistas = fajlbol();
    ELLISTAS_DIJKSTRA_minSSE alg;
    alg.COPY_DATA_INTO_act(ellistas);
    alg.run();
}
    
    //~ for (int i = 0; i < NumOfAct; i++)
    //~ {
        //~ act[i].uid = ellistas[i].uid;
        //~ act[i].Dur_Data = ellistas[i].Dur;
        //~ act[i].Not_Handled_IN_LINKS = ellistas[i].NotHandledPredecessors;
        //~ act[i].Num_Of_IN_LINKS = ellistas[i].NotHandledPredecessors;
        //~ act[i].links_IN.reserve(act[i].Not_Handled_IN_LINKS); act[i].links_IN.resize(ellistas[i].NotHandledPredecessors);
        //~ act[i].Not_Handled_OUT_LINKS= ellistas[i].NotHandledSuccessors;
        //~ act[i].Num_Of_OUT_LINKS= ellistas[i].NotHandledSuccessors;
        //~ act[i].links_OUT.reserve(ellistas[i].NotHandledSuccessors); act[i].links_OUT.resize(ellistas[i].NotHandledSuccessors);
        //~ for (int j = 0; j < ellistas[i].NotHandledPredecessors; j++)
        //~ {
            //~ act[i].links_IN[j].FromNode= ellistas[i].Predecessors[j].FromNode;
            //~ act[i].links_IN[j].LAG = Transform_ONE_LINK_To_SSE_Equivalent(i, j, 0);
        //~ }
        //~ for (int j = 0; j < ellistas[i].NotHandledSuccessors; j++)      
        //~ {
            //~ act[i].links_OUT[j].ToNode = ellistas[i].Successors[j].ToNode;
            //~ act[i].links_OUT[j].LAG = Transform_ONE_LINK_To_SSE_Equivalent(i, j, 1);
        //~ }
    //~ }
}
