#include <cstdlib>

	// act_data* act					= new (act_data[sizeof(act_data) * NumOfAct]);
	
    // act_data* act					= new act_data[NumOfAct];
	
int main() {
    // c
    // dinamikus fogl.    double tömb, 100
    double *ptr;
    ptr = (double*) malloc(sizeof(double) * 100);
    free(ptr);
    
    // c++
    double *ptr2;
    ptr2 = new double[100];
    //         ^típus ^elemszám
    //          automatikus: sizeof(típus) * elemszám
    delete[] ptr2;
   
    // c++, jobb így
    std::vector<act_data> tomb; 
    
    
    // ptr2 = new double[sizeof(double) * 100];     // hibás, 800 bájt helyett 6400 bájt
}
