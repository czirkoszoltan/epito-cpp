//     *******  *******  *        *******  *           * *         ****             ***       ***  **  **    **    **
//	   *        *        *        *        *           * *        *    *            ****     ****  **  **   **     **
//     *        *        *        *        *          *****      *      *           ** **   ** **  **  **  **      **
//     *******  *******  *        *******  *         *     *      *                 **  ** **  **  **  ****        ** 
//     *        *        *        *        *        *       *      ****      *      **   ***   **  **  ****        **
//     *        *        *        *        *        *       *          *            **         **  **  **   **     **
//     *        *        *        *        *         *     *     *      *    *      **         **  **  **    **    **       
//     *        *******  *******  *******  *******    *****       ******            **         **  **  **      **  **

// Activities are non-strecthable, 
// Only MIN PtP relationships are allowed, 
// Activities and links are stored in one combined structure  (ellistas) using vector container

# pragma once
#include <vector>
#include "main.h"
using namespace std;

class ELLISTAS_DIJKSTRA_minSSE
{
	private:
		struct link_IN// Relationships are stored in this structure
		{
			int FromNode=-1;
			int LAG=0;

			link_IN() = default;
		};
		struct link_OUT// Relationships are stored in this structure
		{
			int ToNode=-1;
			int LAG=0;

			link_OUT() = default;
		};
		struct activity
		{
			int uid = 0;		//Unique ID that does not change during the life of the activity
			int Dur_Data = 0;		//Duration for simple time analysis
			int ES = minES_EF; // Initial ES, EF values are set to infinite-1mill
			int LS = maxLS_LF; // Initial ES, EF values are set to infinite-1mill
			int Num_Of_IN_LINKS=0;
			int Num_Of_OUT_LINKS=0;
			int Not_Handled_OUT_LINKS = 0;
			int Not_Handled_IN_LINKS = 0;
			vector<link_OUT> links_OUT;
			vector<link_IN> links_IN;

			activity() = default;					// MEGK�RDEZNI ZOLIT!!!!!!!!!!!!!
			activity(act_ellistas const& data1) // Ez szerintem azt jelenti, hogy az actdata t�mb adataival fogja felt�lteni amikor l�trej�n a p�ld�ny
			{
			this->uid = data1.uid;
			this->Dur_Data = data1.Dur;	// MI�RT NEM M�K�DIK EZ A THIS ->  MEG AZ �SSZES T�
			this->Num_Of_IN_LINKS = data1.NotHandledPredecessors;
			this->Not_Handled_IN_LINKS = data1.NotHandledPredecessors;
			this->Num_Of_OUT_LINKS = data1.NotHandledSuccessors;
			this->Not_Handled_OUT_LINKS = data1.NotHandledSuccessors;
			};

		};
		vector <int> calc;
		vector <activity> act;
		

		void INIT_FOR_RUN();
		void COPY_DATA_INTO_act();
		void TA1st(const int Start_Act, const int Num);
		void TA2nd(const int Num);
		void COPY_DATA_BACK(const int NumOfAct);
		void COPY_DATA_INTO_RES02(const int NumOfAct);
		
	public:
		void DIJKSTRA_MIN_SSE_ELLISTAS();
	    
};
