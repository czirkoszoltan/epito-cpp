// tartalmazás:  használja
// öröklődés: x egyfajta y,   gólya egyfajta madár

class RandomGen {
    public:
        double get_rand() {
            return rand() % 100 / 100.0;     // 0...1
        }
};


#include "randomgen.h"

class BolondParabola {
    private:
        double a, b, c;
        RandomGen r;
    
    public:
        Parabola(double a, double b, double c)
            : a(a), b(b), c(c)
            {
            }
            
        double ertek(double x) {
            return a*x*x + b*x + c + r.get_rand();
        }
};

class BolondParabola2 {
    private:
        double a, b, c;
    
    public:
        BolondParabola2(double a, double b, double c)
            {
                RandomGen r;
                this->a = a + r.get_rand();
                this->b = b + r.get_rand();
                this->c = c + r.get_rand();
            }
            
        double ertek(double x) {
            return a*x*x + b*x + c;
        }
};

int main() {
    Parabola p1(3, -1.2, 4);        // 3 x^2 - 1.2 x + 4
    
    std::cout << p1.ertek(5);
    std::cout << p1.ertek(7);
    
}
