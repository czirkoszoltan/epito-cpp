class FindWindow : public QWindow {
    public:
        FindWindow() {
            this->set_title("Keresés");
        }
};


class MyApp : public QApplication {
    private:
        QButton ok;
  

    public:
        void find_click(QWidget &b) {
            FindWindow w;
            w.run();
        }
        
        void move(QWidget &w, int x, int y);
        
        MyApp() {
            add_event(ok, "click", &ok_click);
            add_event(cancel, "click", &cancel_click);
            add_event(c, "mousemove", &move);
        }
        
        void process_msg(MessageType m, Widget &w) {
            switch (m) {
                case ClickMessage:
                    switch (w) {
                        case ID_OK_BUTTON:
                            ok_click();
                            break;
                        case ID_CANCEL_BUTTON:
                            cancel_click();
                            break;
                    }
                    break;
                    
                case MouseMoveMessage:
                    break;
                case KeyPressMessage:
                    break;
            }
        } 
        
};

int main() {
    MyApp app;
    app.run();
}
